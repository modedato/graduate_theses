## Manhattan plot for EWAS with 
#R
## Author: Marie-Laurence Cossette (based on plots from Steve Horvath's lab and below links)
#https://danielroelfs.com/blog/how-i-create-manhattan-plots-using-ggplot/
#https://www.r-graph-gallery.com/101_Manhattan_plot.html

#set working directory
setwd("/Users/marie/Desktop/SCHOOL/THESIS/Chapter 3 - Methyl/EWAS")

#packages to load
library(dplyr)
library(ggplot2)
library(gghighlight)
library(ggrepel)
library(ggtext)

#load annotation data from Genome_align_probe script
#I renamed the Anno.csv to Anno_sorexcinereus.csv on my computer
Anno_SC <- read.csv("Anno_sorexcinereus.csv")
#remove tarseq_ to only have scaffold number in column
Anno_SC<- Anno_SC%>% mutate(seqnames = as.numeric(gsub("tarseq_", "", seqnames)))
 
#load EWAS data from EWAS script
tab <- read.table("EbFit_sorexcinereus.csv", sep=",", header=TRUE)
#rename column
tab$CGid <- tab$X

#merge EWAS and annotations based on CGid
allData <- merge(tab,Anno_SC, by = "CGid")

#make numeric variables for base pair (where probe starts) and chromosome (scaffold name)
allData$bp <- as.numeric(allData$probeStart)
allData$chr<- as.numeric(allData$seqnames)

#make column with cumulative base pairs
#selects the largest position for each chromosome, and then calculates the cumulative sum of those
data_cum <- allData %>% 
  group_by(chr) %>% 
  summarise(max_bp = max(bp)) %>% 
  mutate(bp_add = lag(cumsum(max_bp), default = 0)) %>% 
  select(chr, bp_add)

#add to main dataset the new column
allData_manhattan <- allData %>% 
  inner_join(data_cum, by = "chr") %>% 
  mutate(bp_cum = bp + bp_add)

#rename p value column
allData_manhattan$p <- allData_manhattan$p.value.Location_1yes
#based on t value ( - or +) make new column indicating ig hyper or hypo methylated
allData_manhattan <- allData_manhattan %>%
  mutate(direction = case_when(allData_manhattan$t.Location_1yes < 0 ~ "neg", allData_manhattan$t.Location_1yes > 0 ~ "pos"))
#save to comp
write.csv(allData_manhattan,file="allData_manhattan_sorexcinereus.csv")

#to set axis ticks to be in middle of each chromosome
axis_set <- allData_manhattan %>% 
  group_by(chr) %>% 
  summarize(center = mean(bp_cum))

#to set y-axis based on your p values
#transform the largest exponent to positive and add 2, to give some extra space on the top edge of the plot
ylim <- allData_manhattan %>% 
  filter(allData_manhattan$p == min(allData_manhattan$p)) %>% 
  mutate(ylim = abs(floor(log10(p))) + 2) %>% 
  pull(ylim)

#indicate the significance threshold
#bonferroni (0.05/24460 total obs)
sig <- 2e-6

#make subsetted data frames for top hypo and hyper methylated sites (for plotting different colors)
significant<- allData_manhattan %>% filter(p < 2e-6)
significant_pos <- subset(significant, direction =="pos")
significant_neg <- subset(significant, direction =="neg")

#actual gene names are in another table, so load it up - functional annotations
functional_ann <- read.table("/Users/marie/Desktop/SCHOOL/THESIS/Chapter 3 - Methyl/functional annotation manh.txt")
#seperating one column into multiple to get final column with only gene name to put on plot
functional_ann <- within(functional_ann, V6<-data.frame(do.call('rbind', strsplit(as.character(V6), '|', fixed=TRUE))))
functional_ann <- within(functional_ann, V6$X3<-data.frame(do.call('rbind', strsplit(as.character(V6$X3), '_', fixed=TRUE))))
functional_ann$gene <- functional_ann$V6$X3$X1
functional_ann$transcriptId <- functional_ann$V1

#join data frame with new one containing functional annotations
allData_manhattan  <- allData_manhattan %>% full_join(functional_ann, by = c("transcriptId"))

#ggplot for manhattan plot
manhplot <- ggplot(allData_manhattan, aes(x = bp_cum, y = -log10(p), color = as_factor(chr), size = -log10(p))) +
  #line for significant p-values
  geom_hline(yintercept = -log10(sig), color = "#f32712", linetype = "dashed") +
  #point color based on chromosome
  geom_point(aes(color=as.factor(chr)), size=0.8) +
  #point color for + and - methylated genes
  geom_point(data = significant_pos, colour = "#fb6869" ,size=1.2) +
  geom_point(data = significant_neg, colour = "#66b1ff",size=1.2) +
  #annotation info for top n (want 15 but 3 don't have gene annotations so select 18)
  geom_text_repel(data=allData_manhattan %>% top_n(-18, p), aes(label=gene), size=5, color ="black") +
  #x axis based on chromosome and place ticks in middle of each chromosome
  scale_x_continuous(label = axis_set$chr, breaks = axis_set$center) +
  #y axis limits set and breaks based on p values
  scale_y_continuous(expand = c(0,0), limits = c(0, ylim), breaks = seq(4, 28, by = 4)) +
  #select colors for non significant plot points
  scale_color_manual(values = rep(c("#bebebe", "#f8e9d5"), unique(length(axis_set$chr)))) +
  #size of non significant plot points
  scale_size_continuous(range = c(0.5)) +
  #axis labels
  labs(x = "Chromosome",y = "-log10(P)", size = 8) + 
  #title 
  ggtitle("Sorex cinereus \n Location") +
  #setting up background lines and white background color 
  theme(panel.background = element_rect(fill = "white", colour = "white",size = 0.5, linetype = "solid"),
  panel.grid.major = element_line(size = 0.25, linetype = 'solid',colour = "#ececec"), 
  panel.grid.minor = element_line(size = 0.15, linetype = 'solid',colour = "#ececec"),
  #no legend
  legend.position = "none",
  #remove extra lines and chromosome numbers, basically just make the plot cleaner
  panel.border = element_blank(),
  panel.grid.major.x = element_blank(),
  panel.grid.minor.x = element_blank(),
  axis.title.y = element_markdown(),
  axis.text.x = element_blank(),
  #position title in the middle and select size
  plot.title = element_text(hjust = 0.5, size = 17)
  )

#plot
plot(manhplot)
