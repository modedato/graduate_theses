01 trimmomatic to remove adapters

input: fastq.gz files R1 and R2 ${1}_R1_001.fastq.gz ${1}_R2_001.fastq.gz
output: trimmed fastq.gz files and files with NAs included ${1}_trim_R1_001.fastq.gz ${1}_NAtrim_R1_001.fastq.gz ${1}_trim_R2_001.fastq.gz ${1}_NAtrim_R2_001.fastq.gz

#### Bash Script filename: 01_trimmomatic.sh
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=trimmomatic
#SBATCH --cpus-per-task=16
#SBATCH --mem=8G
#SBATCH --time=0-06:00 # time (DD-HH:MM)
#SBATCH --mail-user=maegwinbonar@trentu.ca
#SBATCH --mail-type=ALL

module load trimmomatic
#java -jar $EBROOTTRIMMOMATIC/trimmomatic-0.36.jar -threads 16 PE -phred33 ${1}_R1_001.fastq.gz ${1}_R2_001.fastq.gz  ${1}_trim_R1_001.fastq.gz ${1}_NAtrim_R1_001.fastq.gz ${1}_trim_R2_001.fastq.gz ${1}_NAtrim_R2_001.fastq.gz ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36
java -jar $EBROOTTRIMMOMATIC/trimmomatic-0.36.jar PE -threads 16 -phred33 ${1}_R1_001.fastq.gz ${1}_R2_001.fastq.gz ${1}_trim_R1_001.fastq.gz ${1}_NAtrim_R1_001.fastq.gz ${1}_trim_R2_001.fastq.gz ${1}_NAtrim_R2_001.fastq.gz ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36

#### Loop for all fastq files
for f in $(ls *.fastq.gz | cut -f1-3 -d'_'| uniq)
do echo ${f}
sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/raw_fastq/trimmed_slurm/${f}-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/01_trimmomatic.sh ${f}
sleep 10
done

02 map reads, sort, create flagstat

input: trimmed fastq.gz files both R1 and R2 ${1}_trim_R1_001.fastq.gz ${1}_trim_R2_001.fastq.gz
output: alinged reads sam file ${1}_aligned_reads.sam

#### Bash Script filename: 02_bwa.sh
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=bwa
#SBATCH --cpus-per-task=12
#SBATCH --mem=48G
#SBATCH --time=0-12:00 # time (DD-HH:MM)
#SBATCH --mail-user=maegwinbonar@trentu.ca
#SBATCH --mail-type=ALL

module load bwa
echo ${1}

bwa mem -M -t 32 \
/home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/wtd_genome/ovi_v02.1.fasta \
${1}_trim_R1_001.fastq.gz ${1}_trim_R2_001.fastq.gz > /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/${1}_aligned_reads.sam

03 convert sam file to bam file

input: aligned reads same file ${1}_aligned_reads.sam
output: bam file of aligned reads ${1}_aligned_reads.bam

#### Bash Script filename: 03_sam_bam.sh
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=bam-sam
#SBATCH --cpus-per-task=12
#SBATCH --mem=8G
#SBATCH --time=0-08:00 # time (DD-HH:MM)
#SBATCH --mail-user=maegwinbonar@trentu.ca
#SBATCH --mail-type=ALL

module load samtools
echo ${1}

samtools view -bS \
${1}_aligned_reads.sam > ${1}_aligned_reads.bam

04 sort bam file reads

input: unsorted aligned reads bam file ${1}_aligned_reads.bam 
output: sorted algined reads bam file ${1}_sorted_reads.bam

#### Bash Script filename: 04_sort.sh
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=sort
#SBATCH --cpus-per-task=12
#SBATCH --mem=32G
#SBATCH --time=0-08:00 # time (DD-HH:MM)
#SBATCH --mail-user=maegwinbonar@trentu.ca
#SBATCH --mail-type=ALL

module load samtools
echo ${1}
samtools sort -@ 32 \
${1}_aligned_reads.bam > ${1}_sorted_reads.bam

05 Generate qc flagstat

input: sorted bam reads ${1}_sorted_reads.bam
output: flagstat file with qc metrics ${1}_sorted_reads.flagstat

#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=flagstat
#SBATCH --cpus-per-task=8
#SBATCH --mem=8G
#SBATCH --time=0-04:00 # time (DD-HH:MM)
#SBATCH --mail-user=maegwinbonar@trentu.ca
#SBATCH --mail-type=ALL

module load sambamba
echo ${1}
sambamba flagstat \
--nthreads=32 \
${1}_sorted_reads.bam > ${1}_sorted_reads.flagstat

#### loop for all files steps 2-5
#MAP_script_RUN
for f in $(ls *.fastq.gz | cut -f1-3 -d'_'| uniq)
do
echo ${f}
sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/bwa_slurm/${f}-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/02_bwa.sh ${f}
sleep 10
done

for f in $(ls *_aligned_reads.sam | cut -f1-3 -d'_'| uniq)
do
echo ${f}
sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/sam_bam_slurm/${f}-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/03_sam_bam.sh ${f}
sleep 10
done

for f in $(ls *_aligned_reads.bam | cut -f1-3 -d'_'| uniq)
do
echo ${f}
sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/sort_slurm/${f}-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/04_sort.sh ${f}
sleep 10
done

for f in $(ls *_sorted_reads.bam | cut -f1-3 -d'_'| uniq)
do
echo ${f}
sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/flagstat_slurm/${f}-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/05_flagstat.sh ${f}
sleep 10
done

06 Remove duplicate reads

input: sorted reads bam file ${1}_sorted_reads.bam
output: sorted reads with duplications removed ${1}_deduped_reads.bam

#### Bash script filename: 06_picard_dup.sh 
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=remove-dups
#SBATCH --cpus-per-task=32
#SBATCH --mem=125G
#SBATCH --time=0-10:00
#SBATCH --mail-user=maegwinbonar@trentu.ca
#SBATCH --mail-type=ALL

module load picard
module load samtools
module load sambamba
echo ${1}
java -Xmx120G -jar $EBROOTPICARD/picard.jar MarkDuplicates \
INPUT=${1}_sorted_reads.bam \
OUTPUT=${1}_deduped_reads.bam \
USE_JDK_DEFLATER=true USE_JDK_INFLATER=true \
ASSUME_SORT_ORDER=coordinate \
REMOVE_DUPLICATES=true REMOVE_SEQUENCING_DUPLICATES=true \
METRICS_FILE=${1}_deduped.picard 
#END

07 Generate qc flagstat for deduplicated reads

input: deduplicated sorted reads bam file ${1}_deduped_reads.bam 
output: flagstat file with qc metrics ${1}_deduped_reads.flagstat

#### Bash script filename: 07_flagstat_dup.sh
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=remove-dups
#SBATCH --cpus-per-task=8
#SBATCH --mem=8G
#SBATCH --time=0-01:00
#SBATCH --mail-user=maegwinbonar@trentu.ca
#SBATCH --mail-type=ALL

module load sambamba
echo ${1}
sambamba flagstat \
--nthreads=32 \
${1}_deduped_reads.bam > ${1}_deduped_reads.flagstat

##### loop for all files steps 6-7
for f in $(ls *sorted_reads.bam | sed 's/_sorted_reads.bam//' | sort -u)
do
echo ${f}
sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/dups_slurm/${f}-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/06_picard_dup.sh ${f}
sleep 10
done

#loop for all files
for f in $(ls *deduped_reads.bam | sed 's/_deduped_reads.bam//' | sort -u)
do
echo ${f}
sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/dups_slurm/${f}-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/07_flagstat_dup.sh ${f}
sleep 10
done

08 get unique reads script

input: deduped reads bam file ${1}_deduped_reads.bam 
output: unique reads and flagstat file with qc for unique reads ${1}_unique_reads.bam; ${1}_unique_reads.flagstat

#### Bash script filename: 08_unique.sh
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=unique
#SBATCH --cpus-per-task=32
#SBATCH --mem=12G
#SBATCH --time=0-04:00
#SBATCH --mail-user=maegwinbonar@trentu.ca
#SBATCH --mail-type=ALL

module load samtools
module load sambamba
echo ${1}
sambamba view \
--nthreads=32 --with-header --format=bam --show-progress \
-F "mapping_quality >= 1 and not (unmapped or secondary_alignment) and not ([XA] != null or [SA] != null)" \
${1}_deduped_reads.bam \
-o ${1}_unique_reads.bam &&
sambamba flagstat \
--nthreads=32 \
${1}_unique_reads.bam \
> ${1}_unique_reads.flagstat

#loop for all files
for f in $(ls *_deduped_reads.bam | sed 's/_deduped_reads.bam//' | sort -u)
do
echo ${f}
sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/unique_slurm/${f}-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/08_unique.sh ${f}
sleep 10
done

09a get the readgroups for the bam files based on reference genome

input: unique reads bam file ${1}_unique_reads.bam
output: bam file with read groups indicated ${1}_unique_reads_RG.bam

#### Bash script filename: 09a_readgroups.sh
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=read_groups
#SBATCH --cpus-per-task=32
#SBATCH --mem=32G
#SBATCH --time=0-01:00
#SBATCH --mail-user=maegwinbonar@trentu.ca
#SBATCH --mail-type=ALL

echo ${1}
java -jar /cvmfs/soft.computecanada.ca/easybuild/software/2017/Core/picard/2.23.2/picard.jar AddOrReplaceReadGroups \
    I=${1}_unique_reads.bam \
    O=${1}_unique_reads_RG.bam \
    RGID=$(echo ${1} | cut  -d'_' -f2-3) \
    RGLB=10 \
    RGPL=ILLUMINA \
    RGPU=unit1 \
    RGSM=20 \
    SORT_ORDER=coordinate \
    CREATE_INDEX=True

09b realign reads using readgroups bam file

input: unique reads with read groups ${1}_unique_reads_RG.bam
output: realigned bam file ${1}_realigned.bam and intervals ${1}.intervals

#### Bash script filename: 09b_realign.sh
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=realign
#SBATCH --cpus-per-task=32
#SBATCH --mem=32G
#SBATCH --time=04:00:00
#SBATCH --mail-user=maegwinbonar@trentu.ca
#SBATCH --mail-type=ALL

module load gatk/3.8
echo ${1}
java -Xmx30G -jar $EBROOTGATK/GenomeAnalysisTK.jar \
-T RealignerTargetCreator \
-nt 32 \
-R /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/wtd_genome/ovi_v02.1.fasta \
-I ${1}_unique_reads_RG.bam \
-o ${1}.intervals &&
java -Xmx30G -jar $EBROOTGATK/GenomeAnalysisTK.jar \
-T IndelRealigner \
-R /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/wtd_genome/ovi_v02.1.fasta \
-I ${1}_unique_reads_RG.bam \
-targetIntervals ${1}.intervals \
-o ${1}_realigned.bam

#Submit batch scripts
for f in $(ls *_unique_reads_RG.bam | sed 's/_unique_reads_RG.bam//' | sort -u)
do
echo ${f}
sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/realign_slurm/${f}-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/09b_realign.sh ${f}
sleep 10
done

09c realign reads qc flagstat

input: realigned bam file ${1}_realigned.bam
output: flagstat file with qc metrics ${1}_realigned.flagstat

#### Bash script filename: 09c_realign.sh 
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=realign
#SBATCH --cpus-per-task=32
#SBATCH --mem=32G
#SBATCH --time=01:00:00
#SBATCH --mail-user=maegwinbonar@trentu.ca
#SBATCH --mail-type=ALL

module load sambamba
echo ${1}
sambamba flagstat \
--nthreads=32 \
${1}_realigned.bam \
> ${1}_realigned.flagstat

for f in $(ls *_realigned.bam | sed 's/_realigned.bam//' | sort -u)
do
echo ${f}
sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/realign_slurm/${f}-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/09c_realign.sh ${f}
sleep 10
done

10 samtools merge lanes and index seperately if needed - North Magnolia

input: realigned bam files all lanes L001-L005 NM_S1_L001_realigned.bam NM_S1_L002_realigned.bam NM_S1_L003_realigned.bam NM_S1_L004_realigned.bam NM_S1_L005_realigned.bam
output: single indexed file of merged lanes NM_merged_realigned.bam

#### Bash script filename: 10_merge_NM.sh
#!/bin/bash
#SBATCH --time=24:00:00
#SBATCH --account=rrg-shaferab
#SBATCH --cpus-per-task=32
#SBATCH --mem=32G
#SBATCH --job-name=merged_sorted_NM
#SBATCH --output=%x-%j.out
module load samtools
samtools merge --threads 30 --write-index NM_merged_realigned.bam NM_S1_L001_realigned.bam NM_S1_L002_realigned.bam NM_S1_L003_realigned.bam NM_S1_L004_realigned.bam NM_S1_L005_realigned.bam

sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/merge_slurm/NM-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/10_merge_NM.sh


10 samtools merge lanes and index seperately if needed - North Magnolia South

input: realigned bam files all lanes L001-L005 NMS_S1_L001_realigned.bam NMS_S1_L002_realigned.bam NMS_S1_L003_realigned.bam NMS_S1_L004_realigned.bam NMS_S1_L005_realigned.bam
output: single indexed file of merged lanes NMS_merged_realigned.bam

#### Bash script filename: 10_merge_NMS.sh
#!/bin/bash
#SBATCH --time=24:00:00
#SBATCH --account=rrg-shaferab
#SBATCH --cpus-per-task=32
#SBATCH --mem=32G
#SBATCH --job-name=merged_sorted_NMS
#SBATCH --output=%x-%j.out
module load samtools
samtools merge --threads 30 --write-index NMS_merged_realigned.bam NMS_S3_L001_realigned.bam NMS_S3_L002_realigned.bam NMS_S3_L003_realigned.bam NMS_S3_L004_realigned.bam NMS_S3_L005_realigned.bam

sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/merge_slurm/NMS-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/10_merge_NMS.sh

10 samtools merge lanes and index seperately if needed - North Ridge

input: realigned bam files all lanes L001-L005 NR_S1_L001_realigned.bam NR_S1_L002_realigned.bam NR_S1_L003_realigned.bam NR_S1_L004_realigned.bam NR_S1_L005_realigned.bam
output: single indexed file of merged lanes NR_merged_realigned.bam

#### Bash script filename: 10_merge_NR.sh
#!/bin/bash
#SBATCH --time=24:00:00
#SBATCH --account=rrg-shaferab
#SBATCH --cpus-per-task=32
#SBATCH --mem=32G
#SBATCH --job-name=merged_sorted_NR
#SBATCH --output=%x-%j.out
module load samtools
samtools merge --threads 30 --write-index NR_merged_realigned.bam NR_S5_L001_realigned.bam NR_S5_L002_realigned.bam NR_S5_L003_realigned.bam NR_S5_L004_realigned.bam NR_S5_L005_realigned.bam

sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/merge_slurm/NR-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/10_merge_NR.sh


10 samtools merge lanes and index seperately if needed - Ryan Gulch

input: realigned bam files all lanes L001-L005 RG_S1_L001_realigned.bam RG_S1_L002_realigned.bam RG_S1_L003_realigned.bam RG_S1_L004_realigned.bam RG_S1_L005_realigned.bam
output: single indexed file of merged lanes RG_merged_realigned.bam

#### Bash script filename: 10_merge_RG.sh
#!/bin/bash
#SBATCH --time=24:00:00
#SBATCH --account=rrg-shaferab
#SBATCH --cpus-per-task=32
#SBATCH --mem=32G
#SBATCH --job-name=merged_sorted_RG
#SBATCH --output=%x-%j.out
module load samtools
samtools merge --threads 30 --write-index RG_merged_realigned.bam RG_S4_L001_realigned.bam RG_S4_L002_realigned.bam RG_S4_L003_realigned.bam RG_S4_L004_realigned.bam RG_S4_L005_realigned.bam

sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/merge_slurm/RG-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/10_merge_RG.sh


10 samtools merge lanes and index seperately if needed - South Magnolia

input: realigned bam files all lanes L001-L005 SM_S1_L001_realigned.bam SM_S1_L002_realigned.bam SM_S1_L003_realigned.bam SM_S1_L004_realigned.bam SM_S1_L005_realigned.bam
output: single indexed file of merged lanes SM_merged_realigned.bam

#### Bash script filename: 10_merge_SM.sh
#!/bin/bash
#SBATCH --time=24:00:00
#SBATCH --account=rrg-shaferab
#SBATCH --cpus-per-task=32
#SBATCH --mem=32G
#SBATCH --job-name=merged_sorted_SM
#SBATCH --output=%x-%j.out
module load samtools
samtools merge --threads 30 --write-index SM_merged_realigned.bam SM_S2_L001_realigned.bam SM_S2_L002_realigned.bam SM_S2_L003_realigned.bam SM_S2_L004_realigned.bam SM_S2_L005_realigned.bam

sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/merge_slurm/SM-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/10_merge_SM.sh

10b flagstat the merged files

input: merged realigned bam files ${1}_merged_realigned.bam
output: flagstat file with qc ${1}_merged_realigned.flagstat

#### Bash script filename: 10b_flagstat.sh
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=flagstat_merged
#SBATCH --cpus-per-task=32
#SBATCH --mem=32G
#SBATCH --time=01:00:00
#SBATCH --mail-user=maegwinbonar@trentu.ca
#SBATCH --mail-type=ALL

module load sambamba
echo ${1}
sambamba flagstat \
--nthreads=32 \
${1}_merged_realigned.bam \
> ${1}_merged_realigned.flagstat

for f in $(ls *_merged_realigned.bam | sed 's/_merged_realigned.bam//' | sort -u)
do
echo ${f}
sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/flagstat_slurm/merge_${f}-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/10b_flagstat.sh ${f}
sleep 10
done


11 mpileup step to call SNPs

input: all merged and realigned bam files NM_merged_realigned.bam NMS_merged_realigned.bam NR_merged_realigned.bam RG_merged_realigned.bam SM_merged_realigned.bam
output: single mpileup file with calls all_masked_realigned.mpileup

#### Bash script filename: 11_mpileup.sh
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=samtools_mpileup
#SBATCH --output=%x-%j.out
#SBATCH --mem=32G
#SBARCH --cpus-per-task=8
#SBATCH --time=3-00:00
#SBATCH --mail-user=maegwinbonar@trentu.ca
#SBATCH --mail-type=ALL

module load samtools
samtools mpileup \
--fasta-ref /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/MaskedGenome/wtdgenome.masked.fasta \
--ignore-RG -Q 20 \
NM_merged_realigned.bam NMS_merged_realigned.bam NR_merged_realigned.bam RG_merged_realigned.bam SM_merged_realigned.bam \
> all_masked_realigned.mpileup

sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/mpileup_slurm/SM-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/11_mpileup.sh

12a calculating average depth of all covered regions

input: realinged bam files NM_merged_realigned.bam
output: printed out result of depth of coverage for all covered regions Cov2-45161698.out

for this step we need to calculate genome size: https://www.biostars.org/p/356937/
samtools view -H NM_merged_realigned.bam | grep -P '^@SQ' | cut -f 3 -d ':' | awk '{sum+=$1} END {print sum}'

#### Bash script filename: 12a_coverage.sh
#!/bin/bash
#SBATCH --time=8:00:00
#SBATCH --account=rrg-shaferab
#SBATCH --cpus-per-task=2
#SBATCH --mem=4G
#SBATCH --job-name=samtools_depth
#SBATCH --output=%x-%j.out
#SBATCH --mail-user=maegwinbonar@trentu.ca
#SBATCH --mail-type=ALL

module load samtools
samtools depth NM_merged_realigned.bam | awk '{if(($3 > 0) && ($3 < 101)) {print}}' | awk '{sum+=$3; sumsq+=$3*$3} END {print "Average = ",sum/2521034974; print "Stdev = ",sqrt(sumsq/2521034974 - (sum/2521034974)**2)}'
samtools depth NMS_merged_realigned.bam | awk '{if(($3 > 0) && ($3 < 101)) {print}}' | awk '{sum+=$3; sumsq+=$3*$3} END {print "Average = ",sum/2521034974; print "Stdev = ",sqrt(sumsq/2521034974 - (sum/2521034974)**2)}'
samtools depth NR_merged_realigned.bam | awk '{if(($3 > 0) && ($3 < 101)) {print}}' | awk '{sum+=$3; sumsq+=$3*$3} END {print "Average = ",sum/2521034974; print "Stdev = ",sqrt(sumsq/2521034974 - (sum/2521034974)**2)}'
samtools depth RG_merged_realigned.bam | awk '{if(($3 > 0) && ($3 < 101)) {print}}' | awk '{sum+=$3; sumsq+=$3*$3} END {print "Average = ",sum/2521034974; print "Stdev = ",sqrt(sumsq/2521034974 - (sum/2521034974)**2)}'
samtools depth SM_merged_realigned.bam | awk '{if(($3 > 0) && ($3 < 101)) {print}}' | awk '{sum+=$3; sumsq+=$3*$3} END {print "Average = ",sum/2521034974; print "Stdev = ",sqrt(sumsq/2521034974 - (sum/2521034974)**2)}'

sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/coverage_slurm/Cov2-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/12a_coverage.sh

12b calculate depth at each posistion, and proportion of genome to create histogram

input: merged realigned bam file NM_merged_realigned.bam
output: histogram of genome coverage NM_genomecov_hist.txt

#### Bash script filename: 12b_coverage.sh
#!/bin/bash
#SBATCH --time=06:00:00
#SBATCH --account=rrg-shaferab
#SBATCH --cpus-per-task=2
#SBATCH --mem=4G
#SBATCH --job-name=bedtools_coverage
#SBATCH --output=%x-%j.out
#SBATCH --mail-user=maegwinbonar@trentu.ca
#SBATCH --mail-type=ALL

module load bedtools
bedtools genomecov -ibam NM_merged_realigned.bam  -max 101 > NM_genomecov_hist.txt
bedtools genomecov -ibam NMS_merged_realigned.bam -max 101 > NMS_genomecov_hist.txt
bedtools genomecov -ibam NR_merged_realigned.bam -max 101 > NR_genomecov_hist.txt
bedtools genomecov -ibam RG_merged_realigned.bam -max 101 > RG_genomecov_hist.txt
bedtools genomecov -ibam SM_merged_realigned.bam -max 101 > SM_genomecov_hist.txt

sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/coverage_slurm/SM-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/12b_coverage.sh

13 Filter out masked reads from mpileup using sed

input: mpileup file all_masked_realigned.mpileup
output: filtered mpileup file all_masked_filter.mpileup

#### Bash script filename: 13_filtermasked.sh
#!/bin/bash
#SBATCH --time=1:00:00
#SBATCH --account=rrg-shaferab
#SBATCH --cpus-per-task=4
#SBATCH --mem=0
#SBATCH --job-name=filter_masked
#SBATCH --output=%x-%j.out
#SBATCH --mail-user=maegwinbonar@trentu.ca
#SBATCH --mail-type=ALL

sed '/N/d' all_masked_realigned.mpileup > all_masked_filter.mpileup

sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/indel_slurm/filter-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/13_filtermasked.sh

14 Identify and remove indels

input: filtered mpileup file all_masked_filter.mpileup
output: a gtf file aof all indel regions all_indel_regions.gtf and the mpileup file with indels removed all_masked_filter_noindel.mpileup

#### Bash script filename: 14_indel.sh
#!/bin/bash
#SBATCH --time=24:00:00
#SBATCH --account=rrg-shaferab
#SBATCH --cpus-per-task=8
#SBATCH --mem=32G
#SBATCH --job-name=antler_indel
#SBATCH --output=%x-%j.out
#SBATCH --mail-user=maegwinbonar@trentu.ca
#SBATCH --mail-type=ALL

perl /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/popoolation2/popoolation2_1201/indel_filtering/identify-indel-regions.pl --input all_masked_filter.mpileup --output all_indel_regions.gtf --indel-window 5 &&
perl /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/popoolation2/popoolation2_1201/indel_filtering/filter-sync-by-gtf.pl --gtf all_indel_regions.gtf --input all_masked_filter.mpileup --output all_masked_filter_noindel.mpileup

sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/indel_slurm/indel-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/14_indel.sh

15 create java sync file

input: filtered nonindel mpileup file all_masked_filter_noindel.mpileup
output: javasync file pool_final_java.sync

#### Bash script filename: 15_javasync.sh
#!/bin/bash
#SBATCH --time=24:00:00
#SBATCH --account=rrg-shaferab
#SBATCH --cpus-per-task=32
#SBATCH --mem=0
#SBATCH --job-name=HA_LA_sync
#SBATCH --output=%x-%j.out
#SBATCH --mail-user=maegwinbonar@trentu.ca
#SBATCH --mail-type=ALL

java -ea -Xmx120g -jar /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/popoolation2/popoolation2_1201/mpileup2sync.jar --input all_masked_filter_noindel.mpileup --output pool_final_java.sync --fastq-type sanger --min-qual 20 --threads 32

sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/java_slurm/java-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/15_javasync.sh

16 FST in 2500 bp sliding windows

input: java synce file pool_final_java.sync
output fst file pool_2500bp_80.fst

#### Bash script filename: 16_fst_2500.sh
#!/bin/bash
#SBATCH --time=48:00:00
#SBATCH --account=rrg-shaferab
#SBATCH --cpus-per-task=2
#SBATCH --mem=8G
#SBATCH --job-name=2500_fst
#SBATCH --output=%x-%j.out
#SBATCH --mail-user=maegwinbonar@trentu.ca
#SBATCH --mail-type=ALL

perl /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/popoolation2/popoolation2_1201/fst-sliding.pl --input pool_final_java.sync --output pool_2500bp_80.fst --suppress-noninformative --min-count 3 --min-coverage 20 --max-coverage 60 --min-covered-fraction 0.8 --window-size 2500 --step-size 2500 --pool-size 50:33:50:50:50

sbatch -o /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/fst_slurm/fst2500-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/16_fst_2500.sh

#### Then convert the file containing the Fst-values into the .igv format
perl /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/popoolation2/popoolation2_1201/export/pwc2igv.pl --input pool_2500bp_80.fst --output pool_2500bp_80.igv

17 Using the igv file in R select outlier windows using comparison criteria of 4/6 opposite migratory type comparisons in top 1% and not in any of the 4 top 1% of same migratory type comparisons

This is done in R
/MD_poolseq/R/Fst/2-OutlierDetectionFstWindows.R

input: pool_2500bp_80.igv
output: FstOutlierWindows_top67.bed

18 calculate closest genes to windows in bed file

input: sorted bed file FstOutlierWindows_top67.sorted.bed
output: closest genes file Fst67_genes_closest

Make sure the genes gff is sorted properly in wtd gene annotation file
sort -k1,1V -k4,4n -k5,5rn wtd_ME_AUGUSTUS_genes.all.gff > wtd_ME_AUGUSTUS_genes.all.sorted.gff

Make sure the genes in bed file are sorted properly
sort -k1,1V -k2,4n -k3,5rn FstOutlierWindows_top67.bed > FstOutlierWindows_top67.sorted.bed

module load bedtools
bedtools closest -D ref -a FstOutlierWindows_top67.sorted.bed -b /home/mpmb38/projects/rrg-shaferab/mpmb38/Spencer/wtd_ME_AUGUSTUS_genes.all.sorted.gff > Fst67_genes_closest

19 Submsample java sync for windows identified through fst analysis = java sync file is too large to hold in memory as is. Can be done by converting bed file of window coordinates to gtf using:https://github.com/pfurio/bed2gtf

input: full javasync file pool_final_java.sync, gtf file of outlier windows FstOutlierWindows_top50edit.gtf
output: javasync file of only outlier windows Fst_top67.sync 

Here is how to make the gtf file using the bed file of outlier windows
python bed2gtf.py -i FstOutlierWindows_top67.bed -o FstOutlierWindows_top67edit.gtf

Replace all instances where it says peak_id with gene_id instead (dunno why)
sed 's/peak_id/gene_id/g' FstOutlierWindows_top67.gtf > FstOutlierWindows_top67edit.gtf

#### Bash script filename: 19_fst67javasync.sh
#!/bin/bash
#SBATCH --time=2:59:00
#SBATCH --account=rrg-shaferab
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=4G
#SBATCH --job-name=sub_java_sync
#SBATCH --output=%x-%j.out
perl /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/popoolation2/popoolation2_1201/create-genewise-sync.pl --input pool_final_java.sync --gtf /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/Outliers/FstOutlierWindows_top67edit.gtf --output Fst_top67.sync 

sbatch -o subjavasync/Fst67-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/19_fst67javasync.sh

20 Make VCF files for whole genome and Fst outlier windows only

Full genome
input: masked reference genome, merged bam files wtdgenome.masked.fasta NM_merged_realigned.bam NMS_merged_realigned.bam NR_merged_realigned.bam RG_merged_realigned.bam SM_merged_realigned.bam
output: vcf file of whole genome pool_all.vcf

#### Bash script filename: vcf_all.sh
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x-%j.out
#SBATCH --mem=32G
#SBARCH --cpus-per-task=8
#SBATCH --time=3-00:00
#SBATCH --mail-user=maegwinbonar@trentu.ca
#SBATCH --mail-type=ALL
#SBATCH --job-name=bcftools_mpileup

module load bcftools
bcftools mpileup -B -I -Ov -f /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/MaskedGenome/wtdgenome.masked.fasta NM_merged_realigned.bam NMS_merged_realigned.bam NR_merged_realigned.bam RG_merged_realigned.bam SM_merged_realigned.bam > pool_all.vcf

FST outlier windows
input: sorted outlier window bed file, reference genome and merged and realigned bam files FstOutlierWindows_top67.sorted.bed wtdgenome.masked.fasta NM_merged_realigned.bam NMS_merged_realigned.bam NR_merged_realigned.bam RG_merged_realigned.bam SM_merged_realigned.bam
output: vcf file of outlier windows Fst_windows67.vcf

module load bcftools
bcftools mpileup -B -I -Ov -R /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/Outliers/FstOutlierWindows_top67.sorted.bed -f /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/MaskedGenome/wtdgenome.masked.fasta NM_merged_realigned.bam NMS_merged_realigned.bam NR_merged_realigned.bam RG_merged_realigned.bam SM_merged_realigned.bam > Fst_windows67.vcf

21 SNPeff for discovering SNP proximity to geneic regions

#Move protein sequences, annotation and masked reference genome to data bin and save as genes.fa, protein.fa, sequences.fa
#snpEFF for finding mutation effects. Tutorial found: http://snpeff.sourceforge.net/SnpEff_manual.html#intro
#Need to put .gtf annotation, genome, and proteins in a dir labled as genes.gtf sequences.fa and proteins.fa in database and edit configuration file as outlined in tutorial
# wtd genome, version wtd_genome.fasta.masked
wtd : /project/6007974/mpmb38/snpEff_v5.0/snpEff/data/wtd/sequences.fa
wtd.genome : /project/6007974/mpmb38/snpEff_v5.0/snpEff/data/wtd/sequences.fa
mkdir data/wtd/

#build database to reference annotation to
#!/bin/bash
#SBATCH --time=01:00:00
#SBATCH --account=rrg-shaferab
#SBATCH --cpus-per-task=32
#SBATCH --mem=0
#SBATCH --job-name=snpEff_build
#SBATCH --output=%x-%j.out

java -Xmx120G -jar snpEff.jar build -gtf22 -v wtd

Create annotation for vcf file

java -Xmx120G -jar snpEff/snpEff.jar -no INTERGENIC -no INTRAGENIC -ud 25000 -v wtd /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/Fst_windows67.vcf > /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/Fst_windows67.ann.vcf

Extract desired fields, seperating mutliple fields in column by ","

java -jar snpEff/SnpSift.jar extractFields /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/Fst_windows67.ann.vcf -s "," -e "." CHROM POS "ANN[*].EFFECT" "ANN[*].IMPACT" "ANN[*].DISTANCE" "ANN[*].GENE" > /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/md_pool/Fst_windows67.ann.txt


Only take first value in each column, remove everything after column

awk '{sub(/\,.*$/,"",$3)}3' OFS="\t" Fst_windows67.ann.txt | awk '{sub(/\,.*$/,"",$4)}4' OFS="\t" | awk '{sub(/\,.*$/,"",$5)}5' OFS="\t" > Fst_windows67_ANN_single.txt

22 make the bar chart in R

23 make manhattan plot of outlier windows in R

24 make PCA plot of whole genome allele frequencies and PCA plot of outlier window allele frequencies

allele frequency calculations
perl /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/popoolation2/popoolation2_1201/snp-frequency-diff.pl --input pool_final_java.sync --output freq_all --min-count 3 --min-coverage 1 --max-coverage 1000

perl /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/popoolation2/popoolation2_1201/snp-frequency-diff.pl --input Fst_top67.sync --output freq_top67 --min-count 3 --min-coverage 1 --max-coverage 1000

25 Gowinda Gene Ontology
#!/bin/bash
#SBATCH --time=12:00:00
#SBATCH --account=rrg-shaferab
#SBATCH --cpus-per-task=32
#SBATCH --mem=0
#SBATCH --job-name=GOWINDA
#SBATCH --output=%x-%j.out
java -Xmx120G -jar Gowinda-1.12.jar --annotation-file genes.gtf --gene-set-file funcassociate_go_associations_uniprot.txt --snp-file outlier67_snp.bed --candidate-snp-file candidate_snps.bed --gene-definition updownstream2500 --simulations 100000 --min-significance 1 --output-file pool_outlier67_gowinda.txt

sed -i 's/"//g' body_snps_ME.bed


bcftools call -mv all_masked_filter_noindel.mpileup > all_masked_filter_noindel.vcf
bcftools view all_masked_filter_noindel.mpileup

samtools mpileup -uf ref.fa aln.bam | bcftools call -mv > var.raw.vcf

#!/bin/bash
#SBATCH --time=12:00:00
#SBATCH --account=rrg-shaferab
#SBATCH --cpus-per-task=1
#SBATCH --mem=8G
#SBATCH --job-name=AF_test
#SBATCH --output=%x-%j.out
#SBATCH --mail-user=maegwinbonar@trentu.ca
#SBATCH --mail-type=ALL

perl /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/popoolation2/popoolation2_1201/snp-frequency-diff.pl --input pool_final_java.sync --output freq_all --min-count 3 --min-coverage 1 --max-coverage 1000

perl /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/popoolation2/popoolation2_1201/snp-frequency-diff.pl --input Fst_top50.sync --output freq_top50 --min-count 3 --min-coverage 1 --max-coverage 1000

perl /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/popoolation2/popoolation2_1201/snp-frequency-diff.pl --input Fst_top67.sync --output freq_top67 --min-count 3 --min-coverage 1 --max-coverage 1000

sbatch -o java_slurm/java_freq-%A.out /home/mpmb38/projects/rrg-shaferab/mpmb38/poolseq/bash_scripts/java_freq_67.sh

cp freq_top50_rc > freq_temp

cut freq_temp -f1-2 > Ref_loc
cut freq_temp -f10-14 | cut -f1 -d / > MA1
cut freq_temp -f10-14 | cut -f2 | cut -f1 -d / > MA2
cut freq_temp -f10-14 | cut -f3 | cut -f1 -d / > MI1
cut freq_temp  -f10-14 | cut -f4 | cut -f1 -d / > MI2

cut freq_temp -f1-2 > Ref_loc
cut freq_temp -f10-14 | tail -n +2 | cut -f1 -d / > MA1
cut freq_temp -f10-14 | tail -n +2 | cut -f2 | cut -f1 -d / > MA2
cut freq_temp -f10-14 | tail -n +2 | cut -f3 | cut -f1 -d / > MI1
cut freq_temp  -f10-14 | tail -n +2 | cut -f4 | cut -f1 -d / > MI2


module load r
r
install.packages("data.table")
library(data.table)
#paste in CS function at down belo
LOC=read.table("Ref_loc")
MA1=read.table("MA1")
MA2=read.table("MA2")
MI1=read.table("MI1")
MI2=read.table("MI2")


##### PCA
FILE="DEER_raw"
OUT="DEER"
module load nixpkgs/16.09
module load plink/1.9b_5.2-x86_64

#Conduct linkage pruning first 
plink --vcf $FILE.vcf --double-id --allow-extra-chr --indep-pairphase 50 10 0.1 --out $OUT

plink --vcf $FILE.vcf --double-id --allow-extra-chr --extract $OUT.prune.in --make-bed --pca --out $OUT


module load plink/1.9b_5.2-x86_64

#Conduct linkage pruning first 
plink --vcf Fst_windows.vcf --double-id --allow-extra-chr --indep-pairphase 50 10 0.1 --out Fst_windows_clean.vcf

plink --vcf Fst_windows.vcf --double-id --allow-extra-chr --extract Fst_windows.vcf --make-bed --pca --out Fst_windows_pca.vcf



