# Script for the mapping of Deer samples in Graham
# Batch 1-7 comprises 54 samples
# From FastQC to ...

##########################################################################################################
#	SETUP
##########################################################################################################
# path to fastq
fastq=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_FASTQ_raw_files

# path to references
ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files

# path to analysis directory
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B1

root=/home/ckessler/projects/rrg-shaferab/ckessler/



##########################################################################################################
#	PREP 
##########################################################################################################

# index the genome
sbatch -o indexing.out $root/03_index.sh $ref # you can find the adapted 03_index.sh in /Users/camillekessler/Desktop/PhD/Scripts/adapted_Resequencing_SOP

# going to split up the genome into scaffolds to help with run - do before submitting job
mkdir $analysisdir/mpileup_run
awk 'BEGIN {FS="\t"}; {print $1 FS "0" FS $2}' $ref/wtdgenome1.fasta.fai > ./mpileup_coord.bed
awk 'BEGIN {FS="\t"}; {print $1 FS "1" FS $2}' $ref/wtdgenome1.fasta.fai > ./mpileup_coord.list

# Store the slurms in different directories:
mkdir $analysisdir/trimmed_slurm
mkdir $analysisdir/align_slurm
mkdir $analysisdir/dups_slurm
mkdir $analysisdir/unique_slurm
mkdir $analysisdir/clean_slurm

# get sambamba
wget https://github.com/biod/sambamba/releases/download/v0.7.0/sambamba-0.7.0-linux-static.gz
gunzip sambamba-0.7.0-linux-static.gz

##########################################################################################################
#	FastQC 
##########################################################################################################
cd $analysisdir
mkdir fastqc_slurm # to store the slurms
module load fastqc
for f in `ls $fastq/*.fastq.gz`
do
sbatch -o $analysisdir/fastqc_slurm/${f}-%A.out --account=rrg-shaferab --time=02:00:00 --job-name=fastq --wrap="fastqc ${f} -o ${analysisdir}"
done


##########################################################################################################
#	Trimmomatic 
##########################################################################################################

for f in $(ls $fastq/*.fastq.gz | sed "s|$fastq/||" | sed 's/_R[0-9].*//' | uniq)
do
echo ${f}
fastq=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_FASTQ_raw_files
root=/home/ckessler/projects/rrg-shaferab/ckessler/
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B1

sbatch -o $analysisdir/trimmed_slurm/${f}-%A.out $root/02_trimmomaticPE.sh ${f} ${analysisdir} ${fastq}
sleep 10
done

##########
# problems
##########
Odo_VA3_S14_L008 # bwa mem did not work, the 2nd file has fewer sequences. buffer error merging from 0 files
Odo_ON2R_S9_L008 # start over from trimmomatic for both

# resubmit those 06.04.2021
string="Odo_VA3_S14_L008 Odo_ON2R_S9_L008"

for f in $(echo $string | sed 's/ /\n/')
do
echo ${f}

fastq=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_FASTQ_raw_files
root=/home/ckessler/projects/rrg-shaferab/ckessler/
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B1

sbatch -o $analysisdir/redone_slurm/${f}-%A.out $root/02_trimmomaticPE.sh ${f} ${analysisdir} ${fastq}
sleep 10
done

# worked

## Odo_MB1 & Odo_QC2 have a >1 final coverage, remapping to make sure there was no problem
# Odo_QC2_S6_L001
# Odo_MB1_S10_L001

mkdir low_coverage_QC2_MB1 # create dir for previous data
mv Odo_QC2_S6_L001* low_coverage_QC2_MB1 # move previous data
mv Odo_MB1_S10_L001* low_coverage_QC2_MB1 # move previous data

# resubmit  20.04.2021
string="Odo_QC2_S6_L001 Odo_MB1_S10_L001"

for f in $(echo $string | sed 's/ /\n/')
do
echo ${f}

fastq=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_FASTQ_raw_files
root=/home/ckessler/projects/rrg-shaferab/ckessler/
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B1

sbatch -o $analysisdir/redone_slurm/${f}-%A.out $root/02_trimmomaticPE.sh ${f} ${analysisdir} ${fastq}
sleep 10
done



##########################################################################################################
#	bwa_sort 
##########################################################################################################

for f in $(ls $fastq/*.fastq.gz |  sed "s|$fastq/||" | cut -f1-4 -d'_'| uniq) # change cut -f1-4 -d'_' didn't work with Odo_ON_X2_S12_L004_R1_001
do
echo ${f}

fastq=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_FASTQ_raw_files
ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B1
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/redone_slurm/${f}-%A.out $root/04_bwa-sortSE.sh ${f} ${ref}/wtdgenome1.fasta ${fastq} ${analysisdir}
sleep 10
done

##########
# problems
##########

###### reached time limit for 6: #######
ls *tmp* | sed 's/.sorted_reads.bam.*$//' | uniq
MD_CO1_S16_L002
Odo_BC3_S17_L002
Odo_ME1_S11_L002
Odo_QC1_S12_L008
Odo_SK2_S13_L008
Odo_SK3_S8_L008

nano 04_bwa-sortSE.sh # change time to 11hrs
# need to remove tmp for it to work
rm *.bam.tmp*

# resubmit those 09.02.2021
string="MD_CO1_S16_L002 Odo_BC3_S17_L002 Odo_ME1_S11_L002 Odo_QC1_S12_L008 Odo_SK2_S13_L008 Odo_SK3_S8_L008"

for f in $(echo $string | sed 's/ /\n/')
do
echo ${f}

fastq=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_FASTQ_raw_files
ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B1
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/align_slurm/${f}-%A.out $root/04_bwa-sortSE.sh ${f} ${ref}/wtdgenome1.fasta ${fastq} ${analysisdir}
sleep 10
done


###### cut -f1-4 -d'_' didn't work for 4 + 2 downstream malformed files : #######
Odo_ON_X1_S11_L004 # worked
Odo_ON_X2_S12_L004 # worked
Odo_ON_X3_S13_L004 # worked
Odo_ON_X4_S14_L004 # worked

Odo_VA3_S14_L008 # did not work, the 2nd file has fewer sequences. buffer error merging from 0 files
Odo_ON2R_S9_L008 # did not work, will start over from trimmomatic for both

# resubmit those 02.04.2021
string="Odo_ON_X1_S11_L004 Odo_ON_X2_S12_L004 Odo_ON_X3_S13_L004 Odo_ON_X4_S14_L004 Odo_VA3_S14_L008 Odo_ON2R_S9_L008"

for f in $(echo $string | sed 's/ /\n/')
do
echo ${f}

fastq=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_FASTQ_raw_files
ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B1
root=/home/ckessler/projects/rrg-shaferab/ckessler


sbatch -o $analysisdir/redone_slurm/${f}-%A.out $root/04_bwa-sortSE.sh ${f} ${ref}/wtdgenome1.fasta ${fastq} ${analysisdir}
sleep 10
done

###### 2 downstream malformed files that I started over: #######

Odo_VA3_S14_L008 # the 2nd file had fewer sequences. buffer error merging from 0 files
Odo_ON2R_S9_L008 # started over from trimmomatic for both

# resubmit those 02.04.2021
string="Odo_VA3_S14_L008 Odo_ON2R_S9_L008"

for f in $(echo $string | sed 's/ /\n/')
do
echo ${f}

fastq=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_FASTQ_raw_files
ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B1
root=/home/ckessler/projects/rrg-shaferab/ckessler


sbatch -o $analysisdir/redone_slurm/${f}-%A.out $root/04_bwa-sortSE.sh ${f} ${ref}/wtdgenome1.fasta ${fastq} ${analysisdir}
sleep 10
done

# worked

## Odo_MB1 & Odo_QC2 have a >1 final coverage, remapping to make sure there was no problem
# resubmit  20.04.2021
string="Odo_QC2_S6_L001 Odo_MB1_S10_L001"

for f in $(echo $string | sed 's/ /\n/')
do
echo ${f}

fastq=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_FASTQ_raw_files
ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B1
root=/home/ckessler/projects/rrg-shaferab/ckessler


sbatch -o $analysisdir/redone_slurm/${f}-%A.out $root/04_bwa-sortSE.sh ${f} ${ref}/wtdgenome1.fasta ${fastq} ${analysisdir}
sleep 10
done


##########################################################################################################
#	picard
##########################################################################################################

for f in $(ls *.bam | sed 's/.sorted_reads.bam//' | sort -u)
do
echo ${f}
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B1
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/dups_slurm/${f}-%A.out $root/05_picard-dup.sh ${f} ${analysisdir} ${rootdir}
sleep 10
done

##########
# problems
##########
###### cut -f1-4 -d'_' didn't work for 4 + 2 downstream malformed files : #######
Odo_ON_X1_S11_L004 # worked
Odo_ON_X2_S12_L004 # worked
Odo_ON_X3_S13_L004 # worked
Odo_ON_X4_S14_L004 # worked

Odo_VA3_S14_L008 # did not work, the 2nd file has fewer sequences. buffer error merging from 0 files
Odo_ON2R_S9_L008 # did not work,started over from trimmomatic for both

# resubmit those 16.04.2021
string="Odo_ON_X1_S11_L004 Odo_ON_X2_S12_L004 Odo_ON_X3_S13_L004 Odo_ON_X4_S14_L004 Odo_VA3_S14_L008 Odo_ON2R_S9_L008"

for f in $(echo $string | sed 's/ /\n/')
do
echo ${f}

analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B1
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/redone_slurm/${f}-%A.out $root/05_picard-dup.sh ${f} ${analysisdir} ${root}
sleep 10
done

# sambamba didn't work, relaunch only that 
# resubmit sambamba 19.04.2021
string="Odo_ON_X1_S11_L004 Odo_ON_X2_S12_L004 Odo_ON_X3_S13_L004 Odo_ON_X4_S14_L004 Odo_VA3_S14_L008 Odo_ON2R_S9_L008"

for f in $(echo $string | sed 's/ /\n/')
do
echo ${f}

analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B1
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/redone_slurm/${f}-%A.out --account=rrg-shaferab --cpus-per-task=8 --time=0-04:00 --mem=128G --job-name=flagstat --wrap="${root}/sambamba-0.7.0-linux-static flagstat --nthreads=30 ${analysisdir}/${f}.deduped_reads.bam > ${analysisdir}/${f}.deduped_reads.flagstat"
sleep 10
done

## Odo_MB1 & Odo_QC2 have a >1 final coverage, remapping to make sure there was no problem
# resubmit  21.04.2021
string="Odo_QC2_S6_L001 Odo_MB1_S10_L001"
for f in $(echo $string | sed 's/ /\n/')
do
echo ${f}

analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B1
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/redone_slurm/${f}-%A.out $root/05_picard-dup.sh ${f} ${analysisdir} ${root}
sleep 10
done


##########################################################################################################
#	Unique
##########################################################################################################

for f in $(ls *.deduped_reads.bam | sed 's/.deduped_reads.bam//' | sort -u)
do
echo ${f}
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B1
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/unique_slurm/${f}-%A.out $root/06_unique.sh ${f} ${analysisdir} ${root}
sleep 10
done


##########
# problems
##########
###### cut -f1-4 -d'_' didn't work for 4 above, + 2 downstream malformed files : #######
# resubmit those 19.04.2021
string="Odo_ON_X1_S11_L004 Odo_ON_X2_S12_L004 Odo_ON_X3_S13_L004 Odo_ON_X4_S14_L004 Odo_VA3_S14_L008 Odo_ON2R_S9_L008"

for f in $(echo $string | sed 's/ /\n/')
do
echo ${f}

analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B1
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/unique_slurm/${f}-%A.out $root/06_unique.sh ${f} ${analysisdir} ${root}
sleep 10
done

## Odo_MB1 & Odo_QC2 have a >1 final coverage, remapping to make sure there was no problem
# resubmit  21.04.2021
string="Odo_QC2_S6_L001 Odo_MB1_S10_L001"
for f in $(echo $string | sed 's/ /\n/')
do
echo ${f}

analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B1
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/redone_slurm/${f}-%A.out $root/06_unique.sh ${f} ${analysisdir} ${root}
sleep 10
done

##########################################################################################################
#	realign
##########################################################################################################

for f in $(ls *.unique_reads.bam | sed 's/.unique_reads.bam//' | sort -u)
do
echo ${f}

ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B1
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/clean_slurm/${f}-%A.out $root/07_realign.sh ${f} ${ref} ${analysisdir} ${root}

sleep 10
done


##########
# problems
##########
###### cut -f1-4 -d'_' didn't work for 4 above, + 2 downstream malformed files : #######
# resubmit those 19.04.2021
string="Odo_ON_X1_S11_L004 Odo_ON_X2_S12_L004 Odo_ON_X3_S13_L004 Odo_ON_X4_S14_L004 Odo_VA3_S14_L008 Odo_ON2R_S9_L008"

for f in $(echo $string | sed 's/ /\n/')
do
echo ${f}

ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B1
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/redone_slurm/${f}-%A.out $root/07_realign.sh ${f} ${ref} ${analysisdir} ${root}
sleep 10
done

## Odo_MB1 & Odo_QC2 have a >1 final coverage, remapping to make sure there was no problem
# resubmit  21.04.2021
string="Odo_QC2_S6_L001 Odo_MB1_S10_L001"

for f in $(echo $string | sed 's/ /\n/')
do
echo ${f}

ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B1
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/redone_slurm/${f}-%A.out $root/07_realign.sh ${f} ${ref} ${analysisdir} ${root}
sleep 10
done


########
# check, should be 20 each
########
for f in $(ls *.unique_reads.bam | sed 's/.unique_reads.bam//' | sort -u)
do 

echo ${f}
ls ${f}* | wc

done



##########################################################################################################
#	Mosdepth
##########################################################################################################
mkdir mosdepth_slurm

for f in $(ls *.realigned.bam | sed 's/.realigned.bam//' | sort -u)
do
echo ${f}

analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B1
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/mosdepth_slurm/${f}-%A.out --mem=4G --account=rrg-shaferab --time=02:00:00 --job-name=mosdepth --wrap="$root/mosdepth --no-per-base ${f} ${f}.realigned.bam"

sleep 10
done



##########################################################################################################
#	MultiQC
##########################################################################################################
/home/ckessler/ENV/bin/multiqc -f .

rsync -rv ckessler@graham.computecanada.ca:/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B1/multiqc* /Users/camillekessler/Desktop/Dropbox/PhD/Data/multiqc_B1



##########################################################################################################
#	ANGSD
# for all samples, not just this batch
##########################################################################################################
root=/home/ckessler/projects/rrg-shaferab/ckessler
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_ANGSD

ls $root/mapping_B*/*realigned.bam > bam_list 

sbatch -o $analysisdir/ANGSD.out $root/08_ANGSD.sh


#Editing angsd vcf to be compatable with further steps and vcftools we need a new header called vcf_head
#We change from ##fileformat=VCFv4.2(angsd version) to ##fileformat=VCFv4.2
echo "##fileformat=VCFv4.2" > header.txt
module load nixpkgs/16.09
module load intel/2018.3
module load StdEnv/2018.3 
module load tabix

sbatch -o format.out --mem=4G --account=rrg-shaferab --time=02:00:00 --job-name=format --wrap="tabix -r header.txt deer_angsd.vcf.gz > deer_angsd_comp.vcf.gz"

















