# Script for the mapping of Deer samples in Graham
# Batch 10 comrpises 4 samples and is the high coverage batch for PSMC samples
# From FastQC to ...

###############################################################################
#	SETUP
###############################################################################
# path to fastq
fastq=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_FASTQ_Batch10

# path to references
ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files

# path to analysis directory
mkdir mapping_B10
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B10

# root
root=/home/ckessler/projects/rrg-shaferab/ckessler



################################################################################
#	PREP 
################################################################################

# index the genome
sbatch -o indexing.out $root/03_index.sh $ref

# going to split up the genome into scaffolds to help with run - do before submitting job
mkdir $analysisdir/mpileup_run
awk 'BEGIN {FS="\t"}; {print $1 FS "0" FS $2}' $ref/wtdgenome1.fasta.fai > mpileup_run/mpileup_coord.bed
awk 'BEGIN {FS="\t"}; {print $1 FS "1" FS $2}' $ref/wtdgenome1.fasta.fai > mpileup_run/mpileup_coord.list

# Store the slurms in different directories:
mkdir $analysisdir/fastqc_slurm 
mkdir $analysisdir/trimmed_slurm
mkdir $analysisdir/align_slurm
mkdir $analysisdir/dups_slurm
mkdir $analysisdir/unique_slurm
mkdir $analysisdir/clean_slurm

#########################################################################################################
#	FastQC 
#########################################################################################################
cd $analysisdir
module load StdEnv/2016.4
module load fastqc
for f in `ls $fastq/*.fastq.gz`
do

ID=$(echo ${f} | sed "s|$fastq/||" | sed 's/_R[0-9].*//')
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B10

echo ${f}
echo ${ID}
sbatch -o $analysisdir/fastqc_slurm/${ID}-%A.out --account=rrg-shaferab --time=02:00:00 --job-name=fastq --wrap="fastqc ${f} -o ${analysisdir}"
sleep 10
done

# submitted 07.06.2021, 09:36
# BTD_WA1_S8_L008 job 48835191
# BTD_WA1_S8_L008 job 48835309
# MD_NM2_S7_L008 job 48835348
# MD_NM2_S7_L008 job 48835412
# Odo_Key3_S5_L007 job 48835475
# Odo_Key3_S5_L007 job 48835561
# Odo_Key5_S6_L007 job 48835657
# Odo_Key5_S6_L007 job 48835675


#########################################################################################################
#	Trimmomatic 
#########################################################################################################
for f in $(ls $fastq/*.fastq.gz | sed "s|$fastq/||" | sed 's/_R[0-9].*//' | uniq)
do
echo ${f}
fastq=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_FASTQ_Batch10
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B10

sbatch -o $analysisdir/trimmed_slurm/${f}-%A.out $root/02_trimmomaticPE.sh ${f} ${analysisdir} ${fastq}
sleep 10
done

# submitted 07.06.2021, 10:33
# BTD_WA1_S8_L008 job 48840955
# MD_NM2_S7_L008 job 48840958
# Odo_Key3_S5_L007 job 48840966
# Odo_Key5_S6_L007 job 48841044

tail trimmed_slurm/*
ls
#########################################################################################################
#	bwa_sort 
#########################################################################################################

for f in $(ls $fastq/*.fastq.gz |  sed "s|$fastq/||" | sed 's/_R[0-9].*//'| uniq) 
do
echo ${f}

fastq=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_FASTQ_Batch10
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B10
ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/align_slurm/${f}-%A.out $root/04_bwa-sortSE.sh ${f} ${ref}/wtdgenome1.fasta ${fastq} ${analysisdir}
sleep 10
done

# submitted 07.06.2021, 14:33 power outage at night --> relaunch 08.06, 09:13
# BTD_WA1_S8_L008 job 48866076 --> 48886966
# MD_NM2_S7_L008  job 48866078 --> 48886967
# Odo_Key3_S5_L007 job 48866080 --> 48886969
# Odo_Key5_S6_L007 job 48866084 --> 48886970


#*** JOB 48886967 ON gra703 CANCELLED AT 2021-06-08T20:39:25 DUE TO TIME LIMIT ***
#*** JOB 48886966 ON gra703 CANCELLED AT 2021-06-08T20:39:25 DUE TO TIME LIMIT ***

# relaunch both

rm *.bam.tmp*
ls
cat $root/04_bwa-sortSE.sh
nano $root/04_bwa-sortSE.sh # change time to 23hrs

string="BTD_WA1_S8_L008 MD_NM2_S7_L008"

for f in $(echo $string | sed 's/ /\n/')
do
echo ${f}

fastq=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_FASTQ_Batch10
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B10
ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/align_slurm/${f}-%A.out $root/04_bwa-sortSE.sh ${f} ${ref}/wtdgenome1.fasta ${fastq} ${analysisdir}
sleep 10
done

# submitted 09.06.2021, 09:25
# BTD_WA1_S8_L008 job 48921700
# MD_NM2_S7_L008  job 48921701


#########################################################################################################
#	picard
#########################################################################################################
### Add read groups
## Batch 10           
# BTD_WA1_S8_L008       BTD_WA1_S7_L001     B8
# MD_NM2_S7_L008        MD_NM2_S4_L001      B8
# Odo_Key3_S5_L007      Odo_Key3_S3_L001    B9
# Odo_Key5_S6_L007      Odo_Key5_S5_L001    B9

analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B10
B8=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B8
B9=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B9
root=/home/ckessler/projects/rrg-shaferab/ckessler


string="$analysisdir/BTD_WA1_S8_L008 $B8/BTD_WA1_S7_L001 $analysisdir/MD_NM2_S7_L008 $B8/MD_NM2_S4_L001 $analysisdir/Odo_Key3_S5_L007 $B9/Odo_Key3_S3_L001 $analysisdir/Odo_Key5_S6_L007 $B9/Odo_Key5_S5_L001"

module load StdEnv/2016.4
module load picard
module load samtools
module load sambamba

for f in $(echo $string | sed 's/ /\n/')
do
ID=$(echo ${f} | awk -F/ '$0=$NF') # keep only sample ID, -F = separator is /, don't get the '$0=$NF'
if echo "$f" | grep -q "mapping_B10" # if you find mapping_B10
then # then it's the second sequencing run
new_ID=$(echo $analysisdir/${ID}_rgA2) # newfile name with path to analysisdir
RG="2" # read group name
else # otherwise it's the first
new_ID=$(echo $analysisdir/${ID}_rgA1) # newfile name with path to analysisdir
RG="1" # read group name
fi
lib=$(echo ${f} | sed -E 's/[^L]+//') # remove all before first L --> keep library name

echo ${ID}

sbatch -o $analysisdir/dups_slurm/${ID}_addRG-%A.out --account=rrg-shaferab --time=02:00:00 --job-name=add_RG --wrap="java -jar $EBROOTPICARD/picard.jar AddOrReplaceReadGroups I=${f}.sorted_reads.bam  O=${new_ID}.sorted_reads.bam RGID=${RG} RGPU=unit1 RGLB=${lib} RGSM=20 RGPL=ILLUMINA SORT_ORDER=coordinate CREATE_INDEX=True"

done

# launched 10.06.2021, 14:55
# BTD_WA1_S8_L008
# Submitted batch job 48974082
# BTD_WA1_S7_L001
# Submitted batch job 48974083
# MD_NM2_S7_L008
# Submitted batch job 48974084
# MD_NM2_S4_L001
# Submitted batch job 48974085
# Odo_Key3_S5_L007
# Submitted batch job 48974086
# Odo_Key3_S3_L001
# Submitted batch job 48974088
# Odo_Key5_S6_L007
# Submitted batch job 48974089
# Odo_Key5_S5_L001
# Submitted batch job 48974090




### merge files
## Batch 10           
# BTD_WA1_S8_L008       BTD_WA1_S7_L001     B8
# MD_NM2_S7_L008        MD_NM2_S4_L001      B8
# Odo_Key3_S5_L007      Odo_Key3_S3_L001    B9
# Odo_Key5_S6_L007      Odo_Key5_S5_L001    B9

string="BTD_WA1_S8_L008,BTD_WA1_S7_L001 MD_NM2_S7_L008,MD_NM2_S4_L001 Odo_Key3_S5_L007,Odo_Key3_S3_L001 Odo_Key5_S6_L007,Odo_Key5_S5_L001"

module load StdEnv/2016.4
module load picard
module load samtools
module load sambamba

for f in $(echo $string | sed 's/ /\n/')
do
input1=$(echo ${f} | sed 's/^.*,//')
input2=$(echo ${f} | sed 's/,.*$//')
ID=$(echo ${f} | sed 's/_S.*$//')
echo $ID

sbatch -o $analysisdir/dups_slurm/${ID}_merge-%A.out --account=rrg-shaferab --cpus-per-task 4 --mem 16GB --time=11:00:00 --job-name=merge --wrap="java -jar $EBROOTPICARD/picard.jar MergeSamFiles \
      I=${input1}_rgA1.sorted_reads.bam \
      I=${input2}_rgA2.sorted_reads.bam \
      O=${ID}_merged.sorted_reads.bam"

done
# launch 11.06.2021, 09:06
# BTD_WA1
# Submitted batch job 49038705
# MD_NM2
# Submitted batch job 49038706
# Odo_Key3
# Submitted batch job 49038707
# Odo_Key5
# Submitted batch job 49038708
# 


### remove duplicates
for f in $(ls *_merged.sorted_reads.bam | sed 's/.sorted_reads.bam//')
do
echo ${f}
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B10
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/dups_slurm/${f}-%A.out $root/05_picard-dup.sh ${f} ${analysisdir} ${root}
sleep 10
done

# launched 11.06.2021, 11:20
# BTD_WA1_merged
# Submitted batch job 49045056
# MD_NM2_merged
# Submitted batch job 49045066
# Odo_Key3_merged
# Submitted batch job 49045077  *** JOB 49045077 ON gra1136 CANCELLED AT 2021-06-11T15:23:46 DUE TO TIME LIMIT ***
# Odo_Key5_merged
# Submitted batch job 49045087

# change time limit nano ../05_picard-dup.sh to --time=0-11:00 instead of --time=0-04:00
## relaunch Odo_Key3_merged
### remove duplicates
f="Odo_Key3_merged"
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B10
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/dups_slurm/${f}-%A.out $root/05_picard-dup.sh ${f} ${analysisdir} ${root}

# Submitted batch job 49056576 11.06, 15:35


#########################################################################################################
#	Unique
#########################################################################################################

for f in $(ls *.deduped_reads.bam | sed 's/.deduped_reads.bam//' | sort -u)
do
echo ${f}
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B10
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/unique_slurm/${f}-%A.out $root/06_unique.sh ${f} ${analysisdir} ${root}
sleep 10
done

# launched 13.06.2021, 15:12
# BTD_WA1_merged
# Submitted batch job 49114492
# MD_NM2_merged
# Submitted batch job 49114494
# Odo_Key3_merged
# Submitted batch job 49114497
# Odo_Key5_merged
# Submitted batch job 49114499




##########################################################################################################
#	realign
##########################################################################################################
## increase time allocation because of the weight
# 29G BTD_WA1_merged.unique_reads.bam  6.6G Odo_Key3_merged.unique_reads.bam
# 23G MD_NM2_merged.unique_reads.bam    25G Odo_Key5_merged.unique_reads.bam
# change from --time=0-12:00 # time (DD-HH:MM) to --time=0-24:00 # time (DD-HH:MM)

for f in $(ls *.unique_reads.bam | sed 's/.unique_reads.bam//' | sort -u)
do
echo ${f}

analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B10
ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/clean_slurm/${f}-%A.out $root/07_realign.sh ${f} ${ref} ${analysisdir} ${root}

sleep 10
done


# launched 13.06.2021, 16:41
# BTD_WA1_merged
# Submitted batch job 49116286
# MD_NM2_merged
# Submitted batch job 49116287
# Odo_Key3_merged
# Submitted batch job 49116289
# Odo_Key5_merged
# Submitted batch job 49116290


#########################################################################################################
#	Mosdepth
#########################################################################################################
# final coverage
mkdir mosdepth_slurm
for f in $(ls *.realigned.bam | sed 's/.realigned.bam//' | sort -u)
do
echo ${f}

analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B10
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/mosdepth_slurm/${f}-%A.out --mem=4G --account=rrg-shaferab --time=02:00:00 --job-name=mosdepth --wrap="$root/mosdepth --no-per-base ${f} ${f}.realigned.bam"

sleep 10
done

# launch 14.06, 08:36
# BTD_WA1_merged
# Submitted batch job 49132307
# MD_NM2_merged
# Submitted batch job 49132321
# Odo_Key3_merged
# Submitted batch job 49132332
# Odo_Key5_merged
# Submitted batch job 49132353

#########################################################################################################
#	MultiQC
#########################################################################################################
/home/ckessler/ENV/bin/multiqc -f --cl_config log_filesize_limit:2000000000 . # to process large log files, need to increase the max file size
# in local
rsync -rv ckessler@graham.computecanada.ca:/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B10/multiqc* /Users/camillekessler/Desktop/Dropbox/PhD/General_Data


#########################################################################################################
#	Downsample BTD_WA1_merged, MD_NM2_merged & Odo_Key5_merged for regular analysis
# down to ~4.1, the average coverage samples excluding high and bad coverage samples
#########################################################################################################
module load StdEnv/2016.4
module load picard
mkdir downsample_slurm

# BTD_WA1_merged mosdepth coverage = 22, need to downsample to 18% (4.1*100/22)
sbatch -o $analysisdir/downsample_slurm/BTD_WA1-%A.out --cpus-per-task 2 --mem=8G --account=rrg-shaferab --time=06:00:00 --job-name=downsample --wrap="java -Xmx120G -jar $EBROOTPICARD/picard.jar DownsampleSam I=BTD_WA1_merged.realigned.bam O=BTD_WA1_merged_downsampled.realigned.bam P=0.18"
# 49666209

# MD_NM2_merged	mosdepth coverage = 17, need to downsample to 24% (4.1*100/17)
sbatch -o $analysisdir/downsample_slurm/MD_NM2-%A.out --cpus-per-task 2 --mem=8G --account=rrg-shaferab --time=06:00:00 --job-name=downsample --wrap="java -Xmx120G -jar $EBROOTPICARD/picard.jar DownsampleSam I=MD_NM2_merged.realigned.bam O=MD_NM2_merged_downsampled.realigned.bam P=0.24"
# 49666229

# Odo_Key5_merged mosdepth coverage = 15, need to downsample to 27% (4.1*100/15)
sbatch -o $analysisdir/downsample_slurm/Odo_Key5-%A.out --cpus-per-task 2 --mem=8G --account=rrg-shaferab --time=06:00:00 --job-name=downsample --wrap="java -Xmx120G -jar $EBROOTPICARD/picard.jar DownsampleSam I=Odo_Key5_merged.realigned.bam O=Odo_Key5_merged_downsampled.realigned.bam P=0.27"
# 49666239



#########################################################################################################
#	re-Mosdepth
#########################################################################################################
# first, need to index the bam files
for f in $(ls *_downsampled.realigned.bam | sed 's/.realigned.bam//' | sort -u)
do
echo ${f}

analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B10
root=/home/ckessler/projects/rrg-shaferab/ckessler
module load StdEnv/2016.4
module load samtools

sbatch -o $analysisdir/downsample_slurm/${f}_index-%A.out --account=rrg-shaferab --time=02:00:00 --job-name=index --wrap="samtools index ${f}.realigned.bam"

done

# BTD_WA1_merged_downsampled
# Submitted batch job 49686272
# MD_NM2_merged_downsampled
# Submitted batch job 49686273
# Odo_Key5_merged_downsampled
# Submitted batch job 49686274


# final coverage
for f in $(ls *_downsampled.realigned.bam | sed 's/.realigned.bam//' | sort -u)
do
echo ${f}

analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B10
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/downsample_slurm/${f}_mosdepth-%A.out --mem=4G --account=rrg-shaferab --time=02:00:00 --job-name=mosdepth --wrap="$root/mosdepth --no-per-base ${f} ${f}.realigned.bam"

done

# BTD_WA1_merged_downsampled
# Submitted batch job 49686331
# MD_NM2_merged_downsampled
# Submitted batch job 49686332
# Odo_Key5_merged_downsampled
# Submitted batch job 49686333


#########################################################################################################
#	re-MultiQC
#########################################################################################################
/home/ckessler/ENV/bin/multiqc -f --cl_config log_filesize_limit:2000000000 . # to process large log files, need to increase the max file size
# in local
rsync -rv ckessler@graham.computecanada.ca:/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B10/multiqc* /Users/camillekessler/Desktop/Dropbox/PhD/General_Data/multiqc_B10


