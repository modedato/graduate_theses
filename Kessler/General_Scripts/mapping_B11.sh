# Script for the mapping of Deer samples in Graham
# Batch 11 comrpises 7 samples including one high coverage sample and one resequenced samples
# From FastQC to MultiQC

########################################################################################################
#	SETUP
########################################################################################################
# path to fastq
fastq=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_FASTQ_Batch11

# path to references
ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files

# path to analysis directory
mkdir mapping_B11
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B11

# root
root=/home/ckessler/projects/rrg-shaferab/ckessler



########################################################################################################
#	PREP 
########################################################################################################

# going to split up the genome into scaffolds to help with run - do before submitting job
mkdir $analysisdir/mpileup_run
awk 'BEGIN {FS="\t"}; {print $1 FS "0" FS $2}' $ref/wtdgenome1.fasta.fai > mpileup_run/mpileup_coord.bed
awk 'BEGIN {FS="\t"}; {print $1 FS "1" FS $2}' $ref/wtdgenome1.fasta.fai > mpileup_run/mpileup_coord.list

# Store the slurms in different directories:
mkdir $analysisdir/fastqc_slurm 
mkdir $analysisdir/trimmed_slurm
mkdir $analysisdir/align_slurm
mkdir $analysisdir/dups_slurm
mkdir $analysisdir/unique_slurm
mkdir $analysisdir/clean_slurm
mkdir $analysisdir/downsample_slurm
ls ..

########################################################################################################
#	FastQC 
########################################################################################################
cd $analysisdir
module load StdEnv/2016.4
module load fastqc
for f in `ls $fastq/*.fastq.gz`
do

ID=$(echo ${f} | sed "s|$fastq/||" | sed 's/_R[0-9].*//')
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B11

echo ${f}
echo ${ID}
sbatch -o $analysisdir/fastqc_slurm/${ID}-%A.out --account=rrg-shaferab --time=02:00:00 --job-name=fastq --wrap="fastqc ${f} -o ${analysisdir}"
sleep 10
done

# MD_ID2_S4_L001
# Submitted batch job 50049570
# MD_ID2_S4_L001
# Submitted batch job 50049572
# MD_UT2_S6_L001
# Submitted batch job 50049574
# MD_UT2_S6_L001
# Submitted batch job 50049577
# Odo_ON6_S1_L001
# Submitted batch job 50049579
# Odo_ON6_S1_L001
# Submitted batch job 50049581
# Odo_QC2_S5_L001
# Submitted batch job 50049583
# Odo_QC2_S5_L001
# Submitted batch job 50049586
# Odo_SP008_S2_L001
# Submitted batch job 50049589
# Odo_SP008_S2_L001
# Submitted batch job 50049593
# WTD_196_S7_L001
# Submitted batch job 50049599
# WTD_196_S7_L001
# Submitted batch job 50049602
# WTD_ID1_S3_L001
# Submitted batch job 50049606
# WTD_ID1_S3_L001
# Submitted batch job 50049629

tail -n 1 fastqc_slurm/* # all good

########################################################################################################
#	Trimmomatic 
#######################################################################################################
for f in $(ls $fastq/*.fastq.gz | sed "s|$fastq/||" | sed 's/_R[0-9].*//' | uniq)
do
echo ${f}
fastq=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_FASTQ_Batch11
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B11

sbatch -o $analysisdir/trimmed_slurm/${f}-%A.out $root/02_trimmomaticPE.sh ${f} ${analysisdir} ${fastq}
sleep 10
done

# MD_ID2_S4_L001
# Submitted batch job 50050619
# MD_UT2_S6_L001
# Submitted batch job 50050621
# Odo_ON6_S1_L001
# Submitted batch job 50050622
# Odo_QC2_S5_L001
# Submitted batch job 50050623
# Odo_SP008_S2_L001
# Submitted batch job 50050624
# WTD_196_S7_L001
# Submitted batch job 50050626
# WTD_ID1_S3_L001
# Submitted batch job 50050628

tail -n 1 trimmed_slurm/* # all good but one
# ==> trimmed_slurm/MD_ID2_S4_L001-50050619.out <==
# slurmstepd: error: Detected 1 oom-kill event(s) in StepId=50050619.batch cgroup. Some of your processes may have been killed by the cgroup out-of-memory handler.
seff 50050619

module load StdEnv/2016.4
module load trimmomatic

f="MD_ID2_S4_L001"
# double the memory
sbatch -o $analysisdir/trimmed_slurm/${f}-%A.out --account=rrg-shaferab --time=06:00:00 --job-name=trimmomatic --cpus-per-task=4 --mem=16G --wrap="java -jar $EBROOTTRIMMOMATIC/trimmomatic-0.36.jar PE -threads 4 -phred33 ${fastq}/${f}_R1_001.fastq.gz ${fastq}/${f}_R2_001.fastq.gz  ${analysisdir}/${f}_trim_R1_001.fastq.gz ${analysisdir}/${f}_NAtrim_R1_001.fastq.gz ${analysisdir}/${f}_trim_R2_001.fastq.gz ${analysisdir}/${f}_NAtrim_R2_001.fastq.gz ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36"
# Submitted batch job 50053417
seff 50053417

########################################################################################################
#	bwa_sort 
########################################################################################################

for f in $(ls $fastq/*.fastq.gz |  sed "s|$fastq/||" | sed 's/_R[0-9].*//'| uniq) 
do
echo ${f}

fastq=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_FASTQ_Batch11
ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B11

sbatch -o $analysisdir/align_slurm/${f}-%A.out $root/04_bwa-sortSE.sh ${f} ${ref}/wtdgenome1.fasta ${fastq} ${analysisdir}
sleep 10
done

# MD_ID2_S4_L001
# Submitted batch job 50053940
# MD_UT2_S6_L001
# Submitted batch job 50053943
# Odo_ON6_S1_L001
# Submitted batch job 50053944
# Odo_QC2_S5_L001
# Submitted batch job 50053945
# Odo_SP008_S2_L001
# Submitted batch job 50053948
# WTD_196_S7_L001
# Submitted batch job 50053949
# WTD_ID1_S3_L001
# Submitted batch job 50053950

tail -n 1 align_slurm/* # all good


##########################################################################################################
#	picard
##########################################################################################################

### Add read groups for the two samples to merge
## Batch 11  

# Odo_ON6_S1_L001       Odo_ON6_S12_L008     B2
# Odo_QC2_S5_L001       Odo_QC2_S6_L001      B3

analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B11
B1=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B1
root=/home/ckessler/projects/rrg-shaferab/ckessler


string="$analysisdir/Odo_ON6_S1_L001 $B1/Odo_ON6_S12_L008 $analysisdir/Odo_QC2_S5_L001 $B1/Odo_QC2_S6_L001"

module load StdEnv/2016.4
module load picard
module load samtools
module load sambamba

for f in $(echo $string | sed 's/ /\n/g')
do
ID=$(echo ${f} | awk -F/ '$0=$NF') # keep only sample ID, -F = separator is /, don't get the '$0=$NF'
if echo "$f" | grep -q "mapping_B11" # if you find mapping_B10
then # then it's the second sequencing run
new_ID=$(echo $analysisdir/${ID}_rgA2) # newfile name with path to analysisdir
RG="2" # read group name
else # otherwise it's the first
new_ID=$(echo $analysisdir/${ID}_rgA1) # newfile name with path to analysisdir
RG="1" # read group name
fi
lib=$(echo ${f} | sed -E 's/[^L]+//') # remove all before first L --> keep library name

echo ${ID}

sbatch -o $analysisdir/dups_slurm/${ID}_addRG-%A.out --account=rrg-shaferab --time=02:00:00 --mem 4GB --job-name=add_RG --wrap="java -jar $EBROOTPICARD/picard.jar AddOrReplaceReadGroups I=${f}.sorted_reads.bam  O=${new_ID}.sorted_reads.bam RGID=${RG} RGPU=unit1 RGLB=${lib} RGSM=20 RGPL=ILLUMINA SORT_ORDER=coordinate CREATE_INDEX=True"

done

# Odo_ON6_S1_L001
# Submitted batch job 50090209
# Odo_ON6_S12_L008
# Submitted batch job 50090210
# Odo_QC2_S5_L001
# Submitted batch job 50090211
# Odo_QC2_S6_L001
# Submitted batch job 50090212
tail dups_slurm/*

### merge files
## Batch 11           
# Odo_ON6_S1_L001       Odo_ON6_S12_L008     B2
# Odo_QC2_S5_L001       Odo_QC2_S6_L001      B3

string="Odo_ON6_S1_L001,Odo_ON6_S12_L008 Odo_QC2_S5_L001,Odo_QC2_S6_L001"

module load StdEnv/2016.4
module load picard
module load samtools
module load sambamba

for f in $(echo $string | sed 's/ /\n/g')
do
input1=$(echo ${f} | sed 's/^.*,//')
input2=$(echo ${f} | sed 's/,.*$//')
ID=$(echo ${f} | sed 's/_S.*$//')
echo $ID

sbatch -o $analysisdir/dups_slurm/${ID}_merge-%A.out --account=rrg-shaferab --cpus-per-task 4 --mem 16GB --time=11:00:00 --job-name=merge --wrap="java -jar $EBROOTPICARD/picard.jar MergeSamFiles \
      I=${input1}_rgA1.sorted_reads.bam \
      I=${input2}_rgA2.sorted_reads.bam \
      O=${ID}_merged.sorted_reads.bam"

done

# Odo_ON6
# Submitted batch job 50090833
# Odo_QC2
# Submitted batch job 50090834
tail dups_slurm/*_merge*

# remove dups for all samples 
string="MD_ID2_S4_L001 Odo_SP008_S2_L001 MD_UT2_S6_L001 Odo_QC2_merged WTD_196_S7_L001 Odo_ON6_merged WTD_ID1_S3_L001"

for f in $(echo $string | sed 's/ /\n/g')
do
echo ${f}
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B11
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/dups_slurm/${f}_dups-%A.out $root/05_picard-dup.sh ${f} ${analysisdir} ${root}
sleep 10
done

# MD_ID2_S4_L001
# Submitted batch job 50092418
# Odo_SP008_S2_L001
# Submitted batch job 50092419
# MD_UT2_S6_L001
# Submitted batch job 50092420
# Odo_QC2_merged
# Submitted batch job 50092423
# WTD_196_S7_L001
# Submitted batch job 50092424
# Odo_ON6_merged
# Submitted batch job 50092427
# WTD_ID1_S3_L001
# Submitted batch job 50092428
tail dups_slurm/*_dups*

########################################################################################################
#	Unique
########################################################################################################

for f in $(ls *.deduped_reads.bam | sed 's/.deduped_reads.bam//' | sort -u)
do
echo ${f}
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B11
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/unique_slurm/${f}-%A.out $root/06_unique.sh ${f} ${analysisdir} ${root}
sleep 10
done

# MD_ID2_S4_L001
# Submitted batch job 50097124
# MD_UT2_S6_L001
# Submitted batch job 50097129
# Odo_ON6_merged
# Submitted batch job 50097134
# Odo_QC2_merged                  CANCELLED AT 2021-07-15T12:17:44 DUE TO TIME LIMIT ***
# Submitted batch job 50097157
# Odo_SP008_S2_L001
# Submitted batch job 50097160
# WTD_196_S7_L001
# Submitted batch job 50097163
# WTD_ID1_S3_L001
# Submitted batch job 50097174
tail unique_slurm/*

# redo QC2, changed time in 06_unique.sh to 3h
rm Odo_QC2_merged.unique_reads.*

string="Odo_QC2_merged"
for f in $string
do
echo ${f}
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B11
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/unique_slurm/${f}-%A.out $root/06_unique.sh ${f} ${analysisdir} ${root}
sleep 10
done

# Odo_QC2_merged
# Submitted batch job 50126374
########################################################################################################
#	realign
########################################################################################################

for f in $(ls *.unique_reads.bam | sed 's/.unique_reads.bam//' | sort -u)
do
echo ${f}

ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B11
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/clean_slurm/${f}-%A.out $root/07_realign.sh ${f} ${ref} ${analysisdir} ${root}

sleep 10
done

# MD_ID2_S4_L001
# Submitted batch job 50127183
# MD_UT2_S6_L001
# Submitted batch job 50127185
# Odo_ON6_merged
# Submitted batch job 50127186
# Odo_QC2_merged
# Submitted batch job 50127189
# Error: Invalid json-rpc request
# Odo_SP008_S2_L001
# Submitted batch job 50127190
# WTD_196_S7_L001
# Submitted batch job 50127192
# WTD_ID1_S3_L001
# Submitted batch job 50127193
tail clean_slurm/*

########
# check, should be 20 each
########
for f in $(ls *.realigned.bam | sed 's/.realigned.bam//' | sort -u)
do 

echo ${f}
ls ${f}* | wc

done

########################################################################################################
#	Mosdepth
########################################################################################################
mkdir mosdepth_slurm

# final coverage
for f in $(ls *.realigned.bam | sed 's/.realigned.bam//' | sort -u)
do
echo ${f}

analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B11
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/mosdepth_slurm/${f}-%A.out --mem=4G --account=rrg-shaferab --time=02:00:00 --job-name=mosdepth --wrap="$root/mosdepth --no-per-base ${f} ${f}.realigned.bam"

sleep 10
done

# MD_ID2_S4_L001
# Submitted batch job 50132947
# MD_UT2_S6_L001
# Submitted batch job 50132949
# Odo_ON6_merged
# Submitted batch job 50132951
# Odo_QC2_merged
# Submitted batch job 50132953
# Odo_SP008_S2_L001
# Submitted batch job 50132954
# WTD_196_S7_L001
# Submitted batch job 50132956
# WTD_ID1_S3_L001
# Submitted batch job 50132958


#################################################################################################
#	Downsample Odo_ON6_merged for regular analysis
# down to ~4.1, the average coverage samples excluding high and bad coverage samples
#################################################################################################
# gave input as output
# --wrap="java -Xmx120G -jar $EBROOTPICARD/picard.jar DownsampleSam I=Odo_ON6_merged.realigned.bam O=Odo_ON6_merged.realigned.bam P=0.22"
# replaced the realigned file so I need to rerun step 07

for f in $(ls Odo_ON6_merged.unique_reads.bam | sed 's/.unique_reads.bam//' | sort -u)
do
echo ${f}

ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B11
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/clean_slurm/${f}-%A.out $root/07_realign.sh ${f} ${ref} ${analysisdir} ${root}

done
# Odo_ON6_merged
# Submitted batch job 50227968


# Odo_ON6_merged initial mosdepth coverage = 18, need to downsample of 22% (4.1*100/18)
module load StdEnv/2016.4
module load picard
mkdir downsample_slurm

sbatch -o $analysisdir/downsample_slurm/Odo_ON6-%A.out --cpus-per-task 2 --mem=8G --account=rrg-shaferab --time=06:00:00 --job-name=downsample --wrap="java -Xmx120G -jar $EBROOTPICARD/picard.jar DownsampleSam I=Odo_ON6_merged.realigned.bam O=Odo_ON6_merged_dowsampled.realigned.bam P=0.22"
# Submitted batch job 50238369

# index bam file for mosdepth
module load samtools
sbatch -o $analysisdir/downsample_slurm/Odo_ON6_merged_dowsampled_index-%A.out --account=rrg-shaferab --time=02:00:00 --job-name=index --wrap="samtools index Odo_ON6_merged_dowsampled.realigned.bam"
# Submitted batch job 50239695

# mosdepth
for f in $(ls *.realigned.bam | sed 's/.realigned.bam//' | sort -u)
do
echo ${f}

analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B11
root=/home/ckessler/projects/rrg-shaferab/ckessler

sbatch -o $analysisdir/mosdepth_slurm/${f}-%A.out --mem=4G --account=rrg-shaferab --time=02:00:00 --job-name=mosdepth --wrap="$root/mosdepth --no-per-base ${f} ${f}.realigned.bam"

sleep 10
done

# MD_ID2_S4_L001
# Submitted batch job 50239872
# MD_UT2_S6_L001
# Submitted batch job 50239875
# Odo_ON6_merged
# Submitted batch job 50239877
# Odo_ON6_merged_dowsampled
# Submitted batch job 50239880
# Odo_QC2_merged
# Submitted batch job 50239882
# Odo_SP008_S2_L001
# Submitted batch job 50239885
# WTD_196_S7_L001
# Submitted batch job 50239887
# WTD_ID1_S3_L001
# Submitted batch job 50239891

###############################################################################################
#	MultiQC
#############################################################################################
/home/ckessler/ENV/bin/multiqc -f --cl_config log_filesize_limit:2000000000 . # increased max file size


rsync -rv ckessler@graham.computecanada.ca:/home/ckessler/projects/rrg-shaferab/ckessler/mapping_B11/multiqc* /Users/camillekessler/Desktop/Dropbox/PhD/General_Data/multiqc_B11













