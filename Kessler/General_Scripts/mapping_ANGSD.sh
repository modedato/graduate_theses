# ANGSD script
# SNP calling of Deer samples in Graham
# Include vcf file formatting for vctools compatibility


################################################################################
#	ANGSD
# for all samples
################################################################################
root=/home/ckessler/projects/rrg-shaferab/ckessler
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_ANGSD

cd $analysisdir/
ls

## samples that were merged and for which we only keep the merged and downsampled files from batches 10 & 11
# Batch 10: BTD_WA1, MD_NM2, Odo_Key3, Odo_Key5
# Batch 11: Odo_ON6, Odo_QC2, 

# first, create a list of the aligned bam files we don't want to include 
# (bam files from the first sequencing round of each samples that were merged)
ls $root/mapping_final_bam | grep -E 'BTD_WA1|MD_NM2|Odo_Key3|Odo_Key5|Odo_ON6|Odo_QC2' | grep "_L0**" > to_remove_bam
# then create the bam files list wihtout above samples
ls $root/mapping_final_bam/*| grep -vf to_remove_bam > bam_list 

############ ############ ############ ############ ############
# launch ANGSD
sbatch -o $analysisdir/ANGSD.out $root/08_ANGSD.sh
# Submitted batch job 52324852, -killed by the cgroup out-of-memory handler --> change to 8CPU, 128GB
# Submitted batch job 52388568, *** JOB 52388568 ON gra802 CANCELLED AT 2021-09-21T03:33:59 DUE TO TIME LIMIT ***--> change to 16CPU, 64GB, 96h
# Submitted batch job 52508252, killed by the cgroup out-of-memory handler --> change to 8 CPUs, 128GB
# Submitted batch job 52586092

############ ############ ############ ############ ############
#Editing angsd vcf to be compatible with further steps and vcftools we need a new header called vcf_head
#We change from ##fileformat=VCFv4.2(angsd version) to ##fileformat=VCFv4.2
module load StdEnv/2018.3 
module load plink
gunzip -c $analysisdir/demo_data/deer_angsd.vcf.gz  | head -n 1

# test with gunip
echo "##fileformat=VCFv4.2" > header.vcf

#insert new first line with: ##fileformat=VCFv4.2
sbatch -o format.out --cpus-per-task 1 --mem=4G --account=rrg-shaferab --time=04:00:00 --job-name=zip1 --wrap="zcat demo_data/deer_angsd.vcf.gz | sed 1d  > temp.vcf"
# 49593993
sbatch -o format.out --cpus-per-task 1 --mem=4G --account=rrg-shaferab --time=04:00:00 --job-name=zip2 --wrap="cat header.vcf temp.vcf > demo_data/deer_mod_cat_angsd.vcf"
# 49596550
sbatch -o zip.out --cpus-per-task 1 --mem 4G --account=rrg-shaferab --time=15:00:00 --job-name=zip3 --wrap="gzip demo_data/deer_mod_cat_angsd.vcf"
# 49597107

############ ############ ############ ############ ############
## rename individuals in VCF --> from IND01 to Odo_,..
module load StdEnv/2018.3 
module load bcftools
cd $analysisdir

cat bam_list | awk -F/ '$0=$NF' | sed 's/.realigned.bam//' | sed 's/_S[[:digit:]]\+_L00.//' > Samples_list.txt # list with order of samples IDs

# rehaeder in bcftools
sbatch -o indv_named.out --cpus-per-task 1 --mem 4G --account=rrg-shaferab --time=15:00:00 --job-name=indv_named --wrap="bcftools reheader -s $analysisdir/Samples_list.txt $analysisdir/demo_data/deer_mod_cat_angsd.vcf.gz -o $analysisdir/demo_data/deer_mod_cat_angsd_named.vcf.gz"
# 49631244

# check that it worked
bcftools query -l $analysisdir/demo_data/deer_mod_cat_angsd_named.vcf.gz
gunzip -c $analysisdir/demo_data/deer_mod_cat_angsd_named.vcf.gz  | head -n 10 | cut -f 1-10 | column -t # also works with vcf


################################################################################
#	ANGSD
# for 53 WTD samples + 2 downsampled files
################################################################################
root=/home/ckessler/projects/rrg-shaferab/ckessler
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_ANGSD
cd $analysisdir/

# create the bam_list by modyfing the general one
cat bam_list | grep -E "Odo_|WTD_" > bam_list_WTD

# copy ANGSD script and change bam file, output name &nThreads
cp $root/08_ANGSD.sh ./08_ANGSD_WTD.sh
nano 08_ANGSD_WTD.sh
############### ############### ############### ###############
# launch ANGSD
sbatch -o $analysisdir/ANGSD_WTD.out 08_ANGSD_WTD.sh
# Submitted batch job 52327656, lauched on the 16.09.2021, 10:20 -killed by the cgroup out-of-memory handler, change to 8CPU, 128GB
# Submitted batch job 52388766

############ ############ ############ ############ ############
#Editing vcf to be compatible with vcftools we need a new header called vcf_head
# We change from ##fileformat=VCFv4.2(angsd version) to ##fileformat=VCFv4.2
module load StdEnv/2016.4
module load plink
gunzip -c WTD_all_angsd.vcf.gz  | head -n 1

# test with gunip
echo "##fileformat=VCFv4.2" > header.vcf

#insert new first line with: ##fileformat=VCFv4.2
sbatch -o format_WTD.out --cpus-per-task 1 --mem=4G --account=rrg-shaferab --time=04:00:00 --job-name=zip1 --wrap="zcat WTD_all_angsd.vcf.gz | sed 1d  > temp_WTD.vcf"
# Submitted batch job 52508466
sbatch -o format2_WTD.out --cpus-per-task 1 --mem=4G --account=rrg-shaferab --time=04:00:00 --job-name=zip2 --wrap="cat header.vcf temp_WTD.vcf > WTD_all_angsd_vcfCompatible.vcf"
# Submitted batch job 52510168
sbatch -o zip_WTD.out --cpus-per-task 1 --mem 4G --account=rrg-shaferab --time=15:00:00 --job-name=zip3 --wrap="gzip WTD_all_angsd_vcfCompatible.vcf"
# Submitted batch job 52518924
rm temp_WTD.vcf

############ ############ ############ ############ ############
## rename individuals in VCF --> from IND01 to Odo_,..
module load StdEnv/2016.4
module load bcftools

cat bam_list_WTD | awk -F/ '$0=$NF' | sed 's/.realigned.bam//' | sed 's/_S[[:digit:]]\+_L00.//' > Samples_list_WTD.txt # list with order of samples IDs

# rehaeder in bcftools
sbatch -o WTD_named.out --cpus-per-task 1 --mem 4G --account=rrg-shaferab --time=15:00:00 --job-name=indv_named --wrap="bcftools reheader -s Samples_list_WTD.txt WTD_all_angsd_vcfCompatible.vcf.gz -o WTD_all_angsd_vcfCompatible_named.vcf.gz"
# Submitted batch job 52549960

# check that it worked
bcftools query -l WTD_all_angsd_vcfCompatible_named.vcf.gz
gunzip -c WTD_all_angsd_vcfCompatible_named.vcf.gz  | head -n 10 | cut -f 1-10 | column -t 





################################################################################
#	ANGSD
# for 21 MD samples + 2 downsampled files
################################################################################
# create the bam_list by modyfing the general one
cat bam_list | grep -vE "Odo_|WTD_" > bam_list_MD

# copy ANGSD script and change bam file, output name & time limit
cp 08_ANGSD_WTD.sh 08_ANGSD_MD.sh
nano 08_ANGSD_MD.sh
# launch ANGSD
sbatch -o $analysisdir/ANGSD_MD.out 08_ANGSD_MD.sh
# Submitted batch job 52330975, lauched on the 16.09.2021, 11:01

############ ############ ############ ############ ############
#Editing vcf to be compatible with vcftools we need a new header called vcf_head
# We change from ##fileformat=VCFv4.2(angsd version) to ##fileformat=VCFv4.2
gunzip -c MD_all_angsd.vcf.gz   | head -n 1

#insert new first line with: ##fileformat=VCFv4.2
sbatch -o format_MD.out --cpus-per-task 1 --mem=4G --account=rrg-shaferab --time=04:00:00 --job-name=zip1 --wrap="zcat MD_all_angsd.vcf.gz | sed 1d  > temp_MD.vcf"
# Submitted batch job 52508479
sbatch -o format2_MD.out --cpus-per-task 1 --mem=4G --account=rrg-shaferab --time=04:00:00 --job-name=zip2 --wrap="cat header.vcf temp_MD.vcf > MD_all_angsd_vcfCompatible.vcf"
# Submitted batch job 52510186
sbatch -o zip_MD.out --cpus-per-task 1 --mem 4G --account=rrg-shaferab --time=15:00:00 --job-name=zip3 --wrap="gzip MD_all_angsd_vcfCompatible.vcf"
# Submitted batch job 52515435
rm temp_MD.vcf

############ ############ ############ ############ ############
## rename individuals in VCF --> from IND01 to Odo_,..
module load StdEnv/2016.4
module load bcftools

cat bam_list_MD | awk -F/ '$0=$NF' | sed 's/.realigned.bam//' | sed 's/_S[[:digit:]]\+_L00.//' > Samples_list_MD.txt # list with order of samples IDs

# rehaeder in bcftools
sbatch -o MD_named.out --cpus-per-task 1 --mem 4G --account=rrg-shaferab --time=15:00:00 --job-name=indv_named --wrap="bcftools reheader -s Samples_list_MD.txt MD_all_angsd_vcfCompatible.vcf.gz -o MD_all_angsd_vcfCompatible_named.vcf.gz"
# Submitted batch job 52549975


# check that it worked
bcftools query -l MD_all_angsd_vcfCompatible_named.vcf.gz
gunzip -c MD_all_angsd_vcfCompatible_named.vcf.gz  | head -n 10 | cut -f 1-10 | column -t 

