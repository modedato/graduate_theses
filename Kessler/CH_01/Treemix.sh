# Treemix analysis

#based on https://github.com/joepickrell/pophistory-tutorial & https://speciationgenomics.github.io/Treemix/

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 
## Set up 
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 
# set up directory
ch01=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01
#mkdir treemix
treemix=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/treemix
data=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/data
cariboudata=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/data_alignedCaribou
# get python scripts
wget https://github.com/evodify/genotype-files-manipulations/raw/master/calls_to_treeMix_input.py
wget https://github.com/evodify/genotype-files-manipulations/raw/master/calls.py

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 
## input file preparation
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 
# prepare geno file
header1="#CHROM POS" # first part of header
header2=$(cat $data/pops_namevcf.txt | cut -f 1 | paste -s) # get individual names
echo -e $header1"\t"$header2 | sed 's/ \+/\t/g' > header # concatenate

cat header $data/ch01_deer_angsd.geno > $data/ch01_deer_angsd_compatible_named.geno # add to .geno

#get each population idividuals
cat $data/pops_namevcf.txt | awk '{print $2}' | uniq
cat $data/pops_namevcf.txt | grep "WTD_allopatry" | awk '{print $1}' | tr '\n' ','
cat $data/pops_namevcf.txt | grep "WTD_sympatry" | awk '{print $1}' | tr '\n' ','
cat $data/pops_namevcf.txt | grep "MD_allopatry" | awk '{print $1}' | tr '\n' ','
cat $data/pops_namevcf.txt | grep "MD_sympatry" | awk '{print $1}' | tr '\n' ','

# convert named geno file to treemix input
module load python/2.7.18
sbatch -o slurms/geno2treemix-%A.out --account=rrg-shaferab --mem=1G --time=02:00:00 --job-name=geno2treemix --wrap="python calls_to_treeMix_input.py -i $data/ch01_deer_angsd_compatible_named.geno -o ch01_deer_angsd_geno2treemix.out -p 'WTD_allopatry[Odo_ON3,Odo_ON5,Odo_ON6_merged_dowsampled,Odo_PA3,Odo_VA3,Odo_AL3,Odo_SC1];WTD_sympatry[Odo_AB1,Odo_SK2,Odo_SK3,Odo_SD1,Odo_KS1,Odo_MT1,Odo_MX2];MD_allopatry[MD_NV2,MD_CA3,MD_BC6,MD_BC8,BTD_BC1,SD_AK1,BTD_WA1_merged_downsampled];MD_sympatry[MD_AB1,MD_UT2,MD_CO1,MD_SK5,MD_OR2,MD_SD2,MD_NM2_merged_downsampled]'"

# need zipped input
gzip -c ch01_deer_angsd_geno2treemix.out > ch01_deer_angsd_geno2treemix.out.gz

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 
## treemix
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 

# treemix -i ch01_deer_angsd_geno2treemix.out.gz # input
# -m $i # number of migrations
# -o ch01_deer_angsd_m$i # output 
# -root WTD_allopatry # populations to set on one side of the root
# -bootstrap # single bootstrap replicate
# -k 1000 # nb of SNPs per block
# -se # stderr of migration weights

module load StdEnv/2020 gcc/9.3.0 treemix


for i in {0..5}
do
sbatch -o slurms/treemix_deer-%A.out --account=rrg-shaferab --cpus-per-task 4 --mem=16G --time=06:00:00 --job-name=treemix_deer --wrap="treemix -i ch01_deer_angsd_geno2treemix.out.gz -m $i -o ch01_deer_angsd_m$i -root WTD_allopatry -bootstrap -k 1000 -se"
done

# fourpopulation test
sbatch -o slurms/fourpop_deer-%A.out --account=rrg-shaferab --cpus-per-task 3 --mem=12G --time=71:00:00 --job-name=fourpop_deer --wrap="fourpop -i ch01_deer_angsd_geno2treemix.out.gz -k 1000 > ch01_deer_angsd_4poptest.txt"
# ERROR: Line 50544312 has 2 entries. Header has 4
sed -n '50544311,50544313p' ch01_deer_angsd_geno2treemix.out
# unzipped file normal, try re-zipping, works now


# 3population test
sbatch -o slurms/threepop_deer-%A.out --account=rrg-shaferab --cpus-per-task 3 --mem=12G --time=167:00:00 --job-name=threepop_deer --wrap="threepop -i ch01_deer_angsd_geno2treemix.out.gz -k 1000 > ch01_deer_angsd_3poptest.txt"
# ERROR: Line 50544312 has 2 entries. Header has 4 see above
# timeout --> increase time

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 
## treemix with caribou
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 
# prepare input
gunzip $cariboudata/ch01_angsd_mapped_caribou_named.geno.gz
head $cariboudata/ch01_angsd_mapped_caribou_named.geno

module load python/2.7.18
sbatch -o slurms/geno2treemix_alignedCaribou-%A.out --account=rrg-shaferab --mem=1G --time=02:00:00 --job-name=geno2treemix --wrap="python calls_to_treeMix_input.py -i $cariboudata/ch01_angsd_mapped_caribou_named.geno -o ch01_angsd_mapped_caribou_geno2treemix.out -p 'WTD_allopatry[Odo_ON3,Odo_ON5,Odo_ON6,Odo_PA3,Odo_VA3,Odo_AL3,Odo_SC1];WTD_sympatry[Odo_AB1,Odo_SK2,Odo_SK3,Odo_SD1,Odo_KS1,Odo_MT1,Odo_MX2];MD_allopatry[MD_NV2,MD_CA3,MD_BC6,MD_BC8,BTD_BC1,SD_AK1,BTD_WA1];MD_sympatry[MD_AB1,MD_UT2,MD_CO1,MD_SK5,MD_OR2,MD_SD2,MD_NM2];Caribou[GAP-157]'"


# need zipped input
gzip -c ch01_angsd_mapped_caribou_geno2treemix.out > ch01_angsd_mapped_caribou_geno2treemix.out.gz

# treemix
for i in {0..5}
do
sbatch -o slurms/treemix_wCaribou-%A.out --account=rrg-shaferab --cpus-per-task 4 --mem=16G --time=06:00:00 --job-name=treemix_caribou --wrap="treemix -i ch01_angsd_mapped_caribou_geno2treemix.out.gz -m $i -o ch01_angsd_mapped_caribou_m$i -root Caribou -bootstrap -k 1000 -se"
done


# fourpopulation test
sbatch -o slurms/fourpop_caribou-%A.out --account=rrg-shaferab --cpus-per-task 2 --mem=8G --time=167:00:00 --job-name=fourpop_caribou --wrap="fourpop -i ch01_angsd_mapped_caribou_geno2treemix.out.gz -k 1000 > ch01_angsd_mappedCaribou_4poptest.txt"



# fourpopulation test
sbatch -o slurms/threepop_caribou-%A.out --account=rrg-shaferab --cpus-per-task 2 --mem=18G --time=167:00:00 --job-name=threepop_caribou --wrap="threepop -i ch01_angsd_mapped_caribou_geno2treemix.out.gz -k 1000 > ch01_angsd_mappedCaribou_3poptest.txt"

# too many files, put them in resutls dir
mkdir results
mv *_m[0-5].* results/

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 
# get data on local
rsync -vaP ckessler@graham.computecanada.ca:/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/treemix/results /Users/camillekessler/Desktop/Dropbox/PhD/CH_01/Results/treemix

rsync -vaP ckessler@graham.computecanada.ca:/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/treemix/*.txt /Users/camillekessler/Desktop/Dropbox/PhD/CH_01/Results/treemix

## rest in .rmd





##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 
# things that don't work
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 


# remove missing data from vcf with vcf2treemix.sh
module load StdEnv/2020  gcc/9.3.0
module load treemix
module load bcftools

# create clust file with  samplename\tsamplename\tgroup?
bcftools query -l $data/ch01_deer_angsd_compatible_named.vcf > deer.clust
pr -mts' ' deer.clust $data/pops_namevcf.txt > deer.clust

wget https://github.com/speciationgenomics/scripts/raw/master/vcf2treemix.sh
wget https://bitbucket.org/nygcresearch/treemix/downloads/plink2treemix.py
chmod +x vcf2treemix.sh
chmod +x plink2treemix.py

module load vcftools plink/1.9b_6.21-x86_64
./vcf2treemix.sh $data/ch01_deer_angsd_compatible_named.vcf deer.clust
# doesn't work, deletes all SNPs

sbatch -o vcf2treemix.out --account=rrg-shaferab --mem=4G --time=02:00:00 --job-name=vcf2treemix --wrap="./vcf2treemix.sh $data/ch01_deer_angsd_compatible_named.vcf deer.clust"
# Submitted batch job 54340313
# After filtering, kept 0 out of a possible 103970889 Sites
nano vcf2treemix.sh # remove --mac --remove-indels and --max-allele
sbatch -o vcf2treemix-%A.out --account=rrg-shaferab --mem=4G --time=02:00:00 --job-name=vcf2treemix --wrap="./vcf2treemix.sh $data/ch01_deer_angsd_compatible_named.vcf deer.clust"

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 
# remove missing data from vcf with nf-vcf2treemix.git
git clone https://github.com/jbv2/nf-vcf2treemix.git
cd nf-vcf2treemix/
# load needed modules
module load nextflow nixpkgs gcc python plink/2.00-10252019-avx2 StdEnv/2020 intel/2020.1.217 treemix r bcftools/1.9
# need also mk commad in plan9
git clone https://github.com/9fans/plan9port plan9
cd plan9
./INSTALL
cd ..
chmod +x runtest.sh
export PATH=$PATH:/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/treemix/nf-vcf2treemix/plan9/bin/
./runtest.sh

module show plink/1.9b_6.21-x86_64
ls /cvmfs/soft.computecanada.ca/easybuild/modules/2020/Core/plink/1.9b_6.21-x86_64.lua



sbatch -o vcf2treemix.out --account=rrg-shaferab --mem=2G --time=02:00:00 --job-name=vcf2treemix --wrap="nextflow run vcf2treemix.nf --vcffile $data/ch01_deer_angsd_compatible_named.vcf --output_dir $treemix"



### test with other script
wget https://gist.github.com/gibsonMatt/fc456950ab65e6c148797110a1f47262/raw/1138fb9ec18405b7a45dc1502a53e85be4ba766c/vcf2treemix.py
module load python scipy-stack
sbatch -o vcf2treemix_python.out --account=rrg-shaferab --mem=4G --time=06:00:00 --job-name=vcf2treemix --wrap="python vcf2treemix.py $data/ch01_deer_angsd_compatible_named.vcf $data/pops_namevcf.txt ch01_deer_treemix_input"
# Submitted batch job 54266751
# only 0s

# test https://github.com/thomnelson/tools/blob/master/plink2treemix.py
module load plink-1.9-x86_64
# 1st, need clustered allele frequencies file
plink --vcf $data/ch01_deer_angsd_compatible_named.vcf --freq --double-id --allow-extra-chr --family --out test_analysis


git clone --depth 1 https://github.com/grenaud/glactools.git
cd glactools
make
chmod +x glactools/*

glactools view
./glactools vcfm2acf --h vcf file
glactools acf2treemix acf_file

# convert beagle to vcf
wget https://faculty.washington.edu/browning/beagle/beagle.28Jun21.220.jar
module load java
sbatch -o beagle.out --account=rrg-shaferab --mem=4G --time=06:00:00 --job-name=beagle --wrap="java -Xmx4g -jar beagle.28Jun21.220.jar gt=$data/ch01_deer_angsd_compatible_named.vcf out=ch01_deer_angsd_hardcalls"

module load beast beagle-lib
beagle-lib --help




