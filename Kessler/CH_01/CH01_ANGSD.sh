# ANGSD script
# SNP calling of chapter 01, n = 28 deer samples
# three separate launches : all, WTD and MD samples
# Includes vcf file formatting for vctools compatibility and renaming of the individuals

###############################################################################
#	ANGSD
# for all 28 samples
###############################################################################
root=/home/ckessler/projects/rrg-shaferab/ckessler
ch01=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/data

cd $analysisdir/

# create sample info file CH01_info.txt with nano, initially produced in R: CH01_sampling.R 
# create the bam list with paths to the bam files we want to include in ANGSD
# first list all sample files
ls $root/mapping_B*/*realigned.bam > tmp_bam_list
# then, grep to keep only the CH01 samples. For the merged samples, we only keep the downsampled files (BTD_WA1, Odo_ON6, MD_NM2)
{
cat CH01_info.txt | awk  -F"\t" '{print $1}' | # cat the ID column
xargs -i grep {} tmp_bam_list | # grep the ID column into the bam list
# for the merged samples:
grep -Ev 'BTD_WA1_S|MD_NM2_S|Odo_ON6_S' | # remove the first sequencing round
grep -Ev '_merged.realigned.bam' > bam_list # remove the complete merged files
}
cat bam_list | wc # must be 28
rm tmp_bam_list

###############################################################################
# launch ANGSD
cp $root/08_ANGSD.sh .
nano 08_ANGSD.sh # change mem to 128GB & cpus to 32 --> no more large memory, maybe will start soon. saved as 08_ANGSD_ch01.sh
sbatch -o out_slurms/ANGSD.out 08_ANGSD_ch01.sh
# Submitted batch job 50372659
seff 50372659 # used 15.42% of 32 CPUs, 17.28% of 128.00 GB, Wall-clock time: 10:34:29


###############################################################################
#Editing angsd vcf to be compatible with further steps and vcftools 
#We change from ##fileformat=VCFv4.2(angsd version) to ##fileformat=VCFv4.2
module load StdEnv/2018.3 
gunzip -c $analysisdir/deer_angsd.vcf.gz  | head -n 1

# vcf with the header we want
echo "##fileformat=VCFv4.2" > header.vcf
# remove 1st line from our vcf
sbatch -o out_slurms/format1.out --cpus-per-task 1 --mem=4G --account=rrg-shaferab --time=04:00:00 --job-name=zip1 --wrap="zcat deer_angsd.vcf.gz | sed 1d  > temp.vcf"
# Submitted batch job 50470649
head -n 1 temp.vcf
# insert the new line stored in header.vcf
sbatch -o out_slurms/format2.out --cpus-per-task 1 --mem=4G --account=rrg-shaferab --time=04:00:00 --job-name=zip2 --wrap="cat header.vcf temp.vcf > deer_angsd_compatible.vcf"
# Submitted batch job 50471907
head -n 1 deer_angsd_compatible.vcf
rm header.vcf temp.vcf


###############################################################################
# rename individuals in VCF (from IND01 to Odo_,..), individuals in vcf follow the bam_list order
module load StdEnv/2018.3 
module load bcftools
cd $analysisdir
# create an ordered list of samples IDs
cat bam_list | awk -F/ '$0=$NF' | sed 's/.realigned.bam//' | sed 's/_S[[:digit:]]\+_L00.//' > CH01_samples_list.txt 

# rehaeder in bcftools
sbatch -o out_slurms/indv_naming.out --cpus-per-task 1 --mem 4G --account=rrg-shaferab --time=15:00:00 --job-name=indv_naming --wrap="bcftools reheader -s CH01_samples_list.txt deer_angsd_compatible.vcf -o deer_angsd_compatible_named.vcf"
# Submitted batch job 50472462

# check that it worked
bcftools query -l deer_angsd_compatible_named.vcf
cat deer_angsd_compatible_named.vcf  | head -n 10 | cut -f 1-10 | column -t 


###############################################################################
#	ANGSD
# for 14 WTD samples
###############################################################################
# create the bam_list by modyfing the general one
cat bam_list | grep "Odo_" > bam_list_WTD


###############################################################################
# launch ANGSD
# can't make a variable for the diffferent bam lists, will make new scripts instead
cp 08_ANGSD_ch01.sh 08_ANGSD_ch01_WTD.sh  
nano 08_ANGSD_ch01_WTD.sh # change bamlist, output, mem to 32GB & cpus to 8 to match seff 50372659 output
sbatch -o out_slurms/ANGSD_WTD.out 08_ANGSD_ch01_WTD.sh
# Submitted batch job 51321901, COMPLETED in 08:09:33 with 7.22 GB and 44.28% of 8 CPU



###############################################################################
#	ANGSD
# for 14 MD samples
###############################################################################
# create the bam_list by modyfing the general one
cat bam_list | grep -v "Odo_" > bam_list_MD

###############################################################################
# launch ANGSD
cp 08_ANGSD_ch01_WTD.sh 08_ANGSD_ch01_MD.sh  
nano 08_ANGSD_ch01_MD.sh 

sbatch -o out_slurms/ANGSD_MD.out 08_ANGSD_ch01_MD.sh 
# Submitted batch job 51321926, COMPLETED in 07:20:32 with 13.59 GB and 48.55% of 8 CPU

