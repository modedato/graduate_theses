![](Kessler/Images/deer_ppt_template.png)

# Speciation genomics of the _Odocoileus_ genera



## Table of contents

- [Project description](##Project-description)
  - [Chapter 1 - Speciation genomics](### Chapter 1 - Speciation genomics)
  - [Chapter 2 - Demographic history](### Chapter 2 - Demographic history)
  - [Chapter 3 - Ancient DNA](### Chapter 3 - Ancient DNA)
  - [Chapter 4 - Cervid populations history in Northern America](### Chapter 4 - Cervid populations history in Northern America)


## Project description
[(Back to top)](#table-of-contents)

White-tailed deer (_Odocoileus virginianus_; WTD) and mule deer (_O. hemionus_; MD) are two deer sister species endemic to the Americas. They are similar on many aspects of their ecology and life history and are highly abundant in North America, with a density impacting vegetation and predator-prey dynamics. They also represent a high economic value as deer hunting-related activities, and an important cultural component of Indigenous communities across the whole range of the species. 

North America has a dynamic history which shaped the genomic landscape of species. Using modern and ancient DNA, we aim to understand the speciation and demographic history of the _Odocoileus_ genera, as well as its success through time. Using a broader spectrum of species and ancient DNA, we also aim to understand how the climatic and anthropologic changes impacted North American cervid, and if ancient patterns can help current conservation efforts.



### Chapter 1 - Speciation genomics

[(Back to top)](#table-of-contents)

Hybridisation of WTD and MD is highly documented, those two species can reproduce and create fertile hybrids which does not fit in the biological species concept. With modern data from 28 individuals randomly sampled in areas of sympatry and allopatry, we aim to understand the hybridisation of WTD and MD, as well as find signs of genetic isolation and divergence. 


### Chapter 2 - Demographic history
[(Back to top)](#table-of-contents)

Both species are highly successful on the continent, there is evidence of a high resilience of White-tailed deer populations after translocations or introductions on islands, for example. Here, with a dataset of 74 deer covering most of the North American range, we want to understand how and why did those species become so predominant. 

### Chapter 3 - Ancient DNA
[(Back to top)](#table-of-contents)

Building on the first two chapters, we want to further our comprehension of these species evolution by adding an ancient DNA component. Using aDNA will allow us to investigate the impact of the Last Glacial Maximum (LGM), the first human settlement as well as european settlements, on our study species demography and adaptation.



### Chapter 4 - Cervid populations history in Northern America
[(Back to top)](#table-of-contents)

To document and quantify genomic signatures of extinction over time, using ancient and contemporary DNA, we will further investigate the dynamics of Cervidea in Canada through analysis of four additional species: the elk (_Cervus elaphus_), the moose (*Alces alces*), the caribou (*Rangifer tarandus*) and the Torontoceros (*Torontoceros* *hypogeaus*). The history of those species on the continent is highly dynamic and led to very different outcomes: the extinction of the elk and Torontoceros, the current vulnerability of the moose and caribou, and the success of both *Odocoileus* species. 



