## 2019 September 05
## Co-written by Sarah E Haworth & Aaron BA Shafer
## Ontario White-tailed deer (Odocoileus virginianus) Prion Protein Gene (PRNP) SNP Caller
## ----------------------------------------------------------------------------------------------------------

## ----------------------------------------------------------------------------------------------------------
## All file pairs with a file size < 1.0 KB in size in at least one pair was removed before further processing
## ----------------------------------------------------------------------------------------------------------

## ----------------------------------------------------------------------------------------------------------
## Lines to modify yourself:
##	Line 25
##	Line 26
##	Line 44
##	Line 55
##	Line 66
##	Line 77
##	Line 88
##	Line 135
## ----------------------------------------------------------------------------------------------------------

## ----------------------------------------------------------------------------------------------------------
## Naming variables used
## ----------------------------------------------------------------------------------------------------------
reference=Cullingham_ref.fasta
min_length=300

## ----------------------------------------------------------------------------------------------------------
## Loading environment
## environment can be made from miniconda // conda // etc. and will house bwa, samtools, vcftools, etc.
## ----------------------------------------------------------------------------------------------------------
source activate sarhconda

## ----------------------------------------------------------------------------------------------------------
## Index the reference
## ----------------------------------------------------------------------------------------------------------
bwa index ${reference}

## ----------------------------------------------------------------------------------------------------------
## Mapping F1_FP_R1 from R1 to Reference
## ----------------------------------------------------------------------------------------------------------
for file in $(ls *R1_001.fastq.gz | cut -f 1 -d '.');
do
zcat ${file}.fastq.gz | grep "^AC[A/G]TGGGCATATGATGCTGA[C/T]ACCCTCTTTATTTTGCAGATAAGTCATC" -A2 -B1 > ${file}_F1_FP_R1.fastq
pullseq -i ${file}_F1_FP_R1.fastq -m ${min_length} > ${file}_F1_FP_R1.spec.fastq
bwa mem ${reference} ${file}_F1_FP_R1.spec.fastq | samtools view -Sb | samtools sort -T sorted -o ${file}_F1_FP_R1.bam 
samtools depth -aa ${file}_F1_FP_R1.bam > ${file}.sorted_F1_FP_R1.coverage
done

## ----------------------------------------------------------------------------------------------------------
## Mapping F1_RP_R2 from R1 to Reference
## ----------------------------------------------------------------------------------------------------------
for file in $(ls *R2_001.fastq.gz | cut -f 1 -d '.');
do
zcat ${file}.fastq.gz | grep "TCCCCCAACCTGGTAAAGATTAAGAAGATAATGAAAACAGGAAGGTTGCCC" -A2 -B1 > ${file}_F1_RP_R2.fastq
pullseq -i ${file}_F1_RP_R2.fastq -m ${min_length} > ${file}_F1_RP_R2.spec.fastq
bwa mem ${reference} ${file}_F1_RP_R2.spec.fastq | samtools view -Sb | samtools sort -T sorted -o ${file}_F1_RP_R2.bam
samtools depth -aa ${file}_F1_RP_R2.bam > ${file}.sorted_F1_RP_R2.coverage
done

## ----------------------------------------------------------------------------------------------------------
## Mapping F2_FP_R1 from R1 to Reference
## ----------------------------------------------------------------------------------------------------------
for file in $(ls *R1_001.fastq.gz | cut -f 1 -d '.');
do
zcat ${file}.fastq.gz | grep ".\CCCATGGTGGTGGCTGG" -A2 -B1 > ${file}_F2_FP_R1.fastq
pullseq -i ${file}_F2_FP_R1.fastq -m ${min_length} > ${file}_F2_FP_R1.spec.fastq
bwa mem ${reference} ${file}_F2_FP_R1.spec.fastq | samtools view -Sb | samtools sort -T sorted -o ${file}_F2_FP_R1.bam
samtools depth -aa ${file}_F2_FP_R1.bam > ${file}_F2_FP_R1.coverage
done

## ----------------------------------------------------------------------------------------------------------
## Mapping F2_FP_R2 from R2 to Reference
## ----------------------------------------------------------------------------------------------------------
for file in $(ls *R2_001.fastq.gz | cut -f 1 -d '.');
do
zcat ${file}.fastq.gz | grep "CCAGCCACCACCATGGGGCTGACCC" -A2 -B1 > ${file}_F2_FP_R2.fastq
pullseq -i ${file}_F2_FP_R2.fastq -m ${min_length} > ${file}_F2_FP_R2.spec.fastq
bwa mem ${reference} ${file}_F2_FP_R2.spec.fastq | samtools view -Sb | samtools sort -T sorted -o ${file}_F2_FP_R2.bam
samtools depth -aa ${file}_F2_FP_R2.bam > ${file}_F2_FP_R2.coverage
done

## ----------------------------------------------------------------------------------------------------------
## Mapping F2_RP_R2 from R2 to Reference
## ----------------------------------------------------------------------------------------------------------
for file in $(ls *R2_001.fastq.gz | cut -f 1 -d '.');
do
zcat ${file}.fastq.gz | grep "^ACTACAGGGCTGCAGGTAGA[C/T]ACTCCCTCCCCCAACCTGG" -A2 -B1 > ${file}_F2_RP_R2.fastq
pullseq -i ${file}_F2_RP_R2.fastq -m ${min_length} > ${file}_F2_RP_R2.spec.fastq
bwa mem ${reference} ${file}_F2_RP_R2.spec.fastq | samtools view -Sb | samtools sort -T sorted -o ${file}_F2_RP_R2.bam
samtools depth -aa ${file}_F2_RP_R2.bam > ${file}_F2_RP_R2.coverage
done

## ----------------------------------------------------------------------------------------------------------
## Index reference, again
## ----------------------------------------------------------------------------------------------------------
bwa index ${reference}

## ----------------------------------------------------------------------------------------------------------
## Merging files, call SNPs and Generate 2 VCF files and consensus with modified name and hard masked file
## ----------------------------------------------------------------------------------------------------------
for file in $(ls *.bam | cut -f 1-3 -d '_'| uniq);
do
samtools merge ${file}.combined.bam ${file}_R1_001_F1_FP_R1.bam ${file}_R2_001_F1_RP_R2.bam ${file}_R1_001_F2_FP_R1.bam ${file}_R2_001_F2_FP_R2.bam ${file}_R2_001_F2_RP_R2.bam
samtools depth -aa ${file}.combined.bam > ${file}.combined_depth.genome
bcftools mpileup -f ${reference} ${file}.combined.bam | bcftools call -m -Ov -o ${file}.all_raw.vcf
bcftools mpileup -f ${reference} ${file}.combined.bam | bcftools call -m -v -Ov -o ${file}.var_raw.vcf
bcftools view ${file}.all_raw.vcf | ./vcfutils.pl varFilter -Q 30 -d 30 > ${file}.all.vcf
bcftools view ${file}.var_raw.vcf | ./vcfutils.pl varFilter -Q 30 -d 30 > ${file}.var.vcf
bedtools bamtobed -i ${file}.combined.bam > ${file}.combined.bed
bedtools genomecov -bga -i ${file}.combined.bed -g ${file}.combined_depth.genome | grep -w 0$ > ${file}.missing_coverage.bed
bedtools maskfasta -fi ${reference} -bed ${file}.missing_coverage.bed -fo ${file}.masked.fasta
bgzip ${file}.var.vcf
tabix -p vcf ${file}.var.vcf.gz
bgzip ${file}.all.vcf
tabix -p vcf ${file}.all.vcf.gz
cat ${file}.masked.fasta | bcftools consensus -I ${file}.all.vcf.gz > ${file}.all.masked.consensus.fasta
done

## ----------------------------------------------------------------------------------------------------------
## Fix the consensus file name
## ----------------------------------------------------------------------------------------------------------
for file in $(ls *.consensus.fasta);
do
tail -n +2 ${file} > ${file}.mod1
echo ${file} > ${file}.mod2
cat ${file}.mod2 ${file}.mod1 > ${file}.FINAL
sed -i 's/sort/>sort/g' ${file}.FINAL
done
cat *.FINAL > ALL_consensus.fasta

## ----------------------------------------------------------------------------------------------------------
## Ensuring VCF-merge works...you must tell the program where perl library lives
## ----------------------------------------------------------------------------------------------------------
export PERL5LIB=/home/sarh/Downloads/vcftools_0.1.13/perl

## ----------------------------------------------------------------------------------------------------------
## Merge variant VCF files into one file
## ----------------------------------------------------------------------------------------------------------
vcf-merge $(ls -1 *var.vcf.gz | perl -pe 's/\n/ /g') > ALL_variant.vcf
./vcftools --vcf ALL_variant.vcf --extract-FORMAT-info GT 
./vcftools --vcf ALL_variant.vcf --get-INFO REF 
cut -f1-4 out.INFO > out.mod1
cut -f3- out.GT.FORMAT > out.mod2
paste out.mod1 out.mod2 > ALL_variant.GT

## ----------------------------------------------------------------------------------------------------------
## Relocating final files
## ----------------------------------------------------------------------------------------------------------
mkdir FINAL_FILES
mv ALL* FINAL_FILES

## ----------------------------------------------------------------------------------------------------------
## Moving raw files
## ----------------------------------------------------------------------------------------------------------
mkdir VCF_All
mkdir VCF_Var
mkdir FastQ
mkdir Out_Files
mv *.all_raw.vcf VCF_All
mv *.var_raw.vcf VCF_Var
mv *.fastq.gz FastQ
mv out.* Out_Files

## ----------------------------------------------------------------------------------------------------------
## Cleaning up directory
## ----------------------------------------------------------------------------------------------------------
rm *.coverage *.bam *.tbi *.bed *.genome *.spec.fastq *.mod1 *.mod2 *masked* *.fastq *.amb *.ann *.bwt *.fai *.pac *.sa *all.vcf.gz *var.vcf.gz

## ----------------------------------------------------------------------------------------------------------
## END
## ----------------------------------------------------------------------------------------------------------