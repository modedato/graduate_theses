#Part 1 Estimating selection coefficents
#No. of interations
nreps=100
selection_coef = data.frame()
for (i in 1:nreps){
#Model input: allele frequencies pre and post AND number of generations
#Freq for nt285 based on Wilson
#pre=round(runif(1,0.01,0.02), 3)
#post=round(runif(1,0.001,0.009), 3)
#gen=round(runif(1,5,30), 0)
#Freq for nt285 based on Kelly - run after and then rbind
pre=round(runif(1,0.040,0.070), 3)
post=round(runif(1,0.001,0.039), 3)
gen=round(runif(1,5,30), 0)

#Selection values for model optimization - do no change
Sl=0 #lower S
Su=1 # Upper S
Ss=0.0001

#Three relative fitness scenarios
#recessive (sAB = sBB) > Model 1
#codominant (sAA = 2sAB) > Model 2
#dominant (sAA = sAB) -> Model 3

###Optimize S for model 1###
S=Sl
min_dist=1
optS=0
while (S <= Su) {
  p=pre;
  Saa=S;
  Sab=0*S;
  Sbb=0;
  g=1;
  while(g <= gen) {
    H = 2*p*(1-p);
    P = p*p;
    Q = (1-p)*(1-p);
    p = (  (P*(1-Saa)) + (0.5*H*(1-Sab))  ) / (  (P*(1-Saa)) + (H*(1-Sab)) + (Q*(1-Sbb)));
    g=g+1;
  }
  dist=abs(p-post);
  if(dist <= min_dist) {
    min_dist=dist;
    optS=S;
  }
  S=S+Ss;
}
#print(optS)
mod1=optS

###Optimize S for model 2###
S=Sl;
min_dist=1;
optS=0;
while (S <= Su) {
  p=pre;
  Saa=S;
  Sab=0.5*S;
  Sbb=0;
  g=1;
  while(g <= gen) {
    H = 2*p*(1-p);
    P = p*p;
    Q = (1-p)*(1-p);
    p = (  (P*(1-Saa)) + (0.5*H*(1-Sab))  ) / (  (P*(1-Saa)) + (H*(1-Sab)) + (Q*(1-Sbb))  );
    g=g+1;
  }
  dist=abs(p-post);
  if(dist <= min_dist) {
    min_dist=dist;
    optS=S;
  }
  S=S+Ss;
}
#print(optS)
mod2=optS

###Optimize S for model 3###
S=Sl;
min_dist=1;
optS=0;
while (S <= Su) {
  p=pre;
  Saa=S;
  Sab=1*S;
  Sbb=0;
  g=1;
  while(g <= gen) {
    H = 2*p*(1-p);
    P = p*p;
    Q = (1-p)*(1-p);
    p = (  (P*(1-Saa)) + (0.5*H*(1-Sab))  ) / (  (P*(1-Saa)) + (H*(1-Sab)) + (Q*(1-Sbb))  );
    g=g+1;
  }
  dist=abs(p-post);
  if(dist <= min_dist) {
    min_dist=dist;
    optS=S;
  }
  S=S+Ss;
}
#print(optS)
mod3=optS

model=cbind(mod1,mod2,mod3)
df <- data.frame(model)
selection_coef <- rbind(selection_coef,df)
}
wilson<-selection_coef
selection_coef<-rbind(selection_coef,wilson)
#Make table with summary stats
library(magrittr)
library(qwraps2)
library(dplyr)
options(qwraps2_markup="markdown")
Sel_summary <-
  list("Model1" =
         list("min" = ~ min(.data$mod1),
              "max" = ~ max(.data$mod1),
              "mean (sd)" = ~ qwraps2::mean_sd(.data$mod1)),
       "Model2" =
         list("min" = ~ min(.data$mod2),
              "max" = ~ max(.data$mod2),
              "mean (sd)" = ~ qwraps2::mean_sd(.data$mod2)),
       "Model3" =
         list("min" = ~ min(.data$mod3),
              "max" = ~ max(.data$mod3),
              "mean (sd)" = ~ qwraps2::mean_sd(.data$mod3))
       )

TableX <- summary_table(selection_coef, Sel_summary)
TableX

#PART 2
#Propragate AF forward using S coefficients
#equation to run recursively over X geneerations
#p′ = (sSS p2 + sSF p(1 − p)/(sSS p2 + sSF 2p(1 − p) + sFF (1 − p)2)
#note it's 1 - s 

#user input
Start_Freq=0.97
ngen=25

#model1 - recessive
m <- matrix(0, ncol = 0, nrow = 1)
mod1_AF = data.frame() 
df2 = data.frame()
for (s in selection_coef$mod1){
  p=Start_Freq
  q=1-p
  Saa=1-(s);
  Sab=1-(0*Saa);
  Sbb=1-0;
  i=1
  mod1_AF_temp = data.frame() 
  while (i <= ngen){
    p_next=(Saa*(p^2))+((Sab*p*q)/(Saa*(p^2)+Sab*(2*p*q)+Sbb*(q^2)))
    p=p_next
    q=1-p
    df2<- data.frame(p)
    df2<-cbind(i,df2)
    mod1_AF_temp <- rbind(mod1_AF_temp,df2)
    i=i+1
    }
  #names(mod1_AF_temp) <- c(s)
  df3 <- data.frame(mod1_AF_temp)
  mod1_AF <- rbind(mod1_AF,df3)
}  



#model2 - codominant
m <- matrix(0, ncol = 0, nrow = 1)
mod2_AF = data.frame() 
df2 = data.frame()
for (s in selection_coef$mod2){
  p=Start_Freq
  q=1-p
  Saa=1-(s);
  Sab=1-(0.5*Saa);
  Sbb=1-0;
  i=1
  mod2_AF_temp = data.frame() 
  while (i <= ngen){
    p_next=(Saa*(p^2))+((Sab*p*q)/(Saa*(p^2)+Sab*(2*p*q)+Sbb*(q^2)))
    p=p_next
    q=1-p
    df2<- data.frame(p)
    df2<-cbind(i,df2)
    mod2_AF_temp <- rbind(mod2_AF_temp,df2)
    i=i+1
  }
  #names(mod1_AF_temp) <- c(s)
  df3 <- data.frame(mod2_AF_temp)
  mod2_AF <- rbind(mod2_AF,df3)
} 
#model3 - dominant
m <- matrix(0, ncol = 0, nrow = 1)
mod3_AF = data.frame() 
df2 = data.frame()
for (s in selection_coef$mod3){
  p=Start_Freq
  q=1-p
  Saa=1-(s);
  Sab=Saa;
  Sbb=1-0;
  i=1
  mod3_AF_temp = data.frame() 
  while (i <= ngen){
    p_next=(Saa*(p^2))+((Sab*p*q)/(Saa*(p^2)+Sab*(2*p*q)+Sbb*(q^2)))
    p=p_next
    q=1-p
    df2<- data.frame(p)
    df2<-cbind(i,df2)
    mod3_AF_temp <- rbind(mod3_AF_temp,df2)
    i=i+1
  }
  #names(mod1_AF_temp) <- c(s)
  df3 <- data.frame(mod3_AF_temp)
  mod3_AF <- rbind(mod3_AF,df3)
} 


#Add gen 0 
i=0
p=Start_Freq
df_null=data.frame(p)
df_null=cbind(i,df_null)

mod1_AF=rbind(mod1_AF,df_null)
mod2_AF=rbind(mod2_AF,df_null)
mod3_AF=rbind(mod3_AF,df_null)

#Models for mean(s)
#model1 - recessive low 
m <- matrix(0, ncol = 0, nrow = 1)
mod1_AF_mean = data.frame() 
df2 = data.frame()
s=mean(selection_coef$mod1)
p=Start_Freq
q=1-p
Saa=1-(s);
Sab=1-(0*Saa);
Sbb=1-0;
i=1
mod1_AF_temp = data.frame() 
while (i <= ngen){
  p_next=(Saa*(p^2))+((Sab*p*q)/(Saa*(p^2)+Sab*(2*p*q)+Sbb*(q^2)))
  p=p_next
  q=1-p
  df2<- data.frame(p)
  df2<-cbind(i,df2)
  mod1_AF_temp <- rbind(mod1_AF_temp,df2)
  i=i+1
}
#names(mod1_AF_temp) <- c(s)
mod1_AF_mean <- data.frame(mod1_AF_temp)

#Models for Robinson
#model3 - recessive low s
m <- matrix(0, ncol = 0, nrow = 1)
mod3_AF_rob1 = data.frame() 
df2 = data.frame()
s=0.01
p=Start_Freq
q=1-p
Saa=1-(s);
Sab=Saa;
Sbb=1-0;
i=1
mod3_AF_temp = data.frame() 
while (i <= ngen){
  p_next=(Saa*(p^2))+((Sab*p*q)/(Saa*(p^2)+Sab*(2*p*q)+Sbb*(q^2)))
  p=p_next
  q=1-p
  df2<- data.frame(p)
  df2<-cbind(i,df2)
  mod3_AF_temp <- rbind(mod3_AF_temp,df2)
  i=i+1
}
#names(mod1_AF_temp) <- c(s)
mod3_AF_rob1 <- data.frame(mod3_AF_temp)

#model1 - recessive high s  
  m <- matrix(0, ncol = 0, nrow = 1)
  mod3_AF_rob2 = data.frame() 
  df2 = data.frame()
  s=0.074
  p=Start_Freq
  q=1-p
  Saa=1-(s);
  Sab=Saa;
  Sbb=1-0;
  i=1
  mod3_AF_temp = data.frame() 
  while (i <= ngen){
    p_next=(Saa*(p^2))+((Sab*p*q)/(Saa*(p^2)+Sab*(2*p*q)+Sbb*(q^2)))
    p=p_next
    q=1-p
    df2<- data.frame(p)
    df2<-cbind(i,df2)
    mod3_AF_temp <- rbind(mod3_AF_temp,df2)
    i=i+1
  }
  #names(mod1_AF_temp) <- c(s)
  mod3_AF_rob2 <- data.frame(mod3_AF_temp)
  
  library(ggplot2)
  #Individual plots
  mod1 <- ggplot(mod1_AF, aes(i, p)) + geom_point() + geom_smooth(method = loess)
  mod1
  mod1 + geom_hline(yintercept=0.97, linetype="dashed", color = "black") + geom_line(aes(i,p),mod1_AF_rob2,color="red") + geom_line(aes(i,p),mod1_AF_rob1,color="green")
  
  mod3 <- ggplot(mod3_AF, aes(i, p)) + geom_point() + geom_smooth(method = loess)
  mod3
  mod3 + geom_hline(yintercept=0.97, linetype="dashed", color = "black") + geom_line(aes(i,p),mod3_AF_rob2,color="red") + geom_line(aes(i,p),mod3_AF_rob1,color="green")
  
  
  #STOP FOR NOW
  
  mod2 <- ggplot(mod2_AF, aes(i, p)) + geom_point() + geom_smooth(method = loess)
  mod2
  mod3 <- ggplot(mod3_AF, aes(i, p)) + geom_point() + geom_smooth(method = loess)
  mod3
  
  
  #Merge all datasets
  mod1_AF$model=rep("recessive",nreps*ngen+1)
  mod2_AF$model=rep("codominant",nreps*ngen+1)
  mod3_AF$model=rep("dominant",nreps*ngen+1)
  
  comb_mod=rbind(mod1_AF,mod2_AF,mod3_AF)
  
  ggplot(data = comb_mod, mapping = aes(x = i, y = p, color = model)) + geom_line() 
  all=ggplot(data = comb_mod, aes(x = i, y = p, col = factor(model))) + stat_smooth(method = "loess", se = T, fullrange = T)+geom_point()
  all+ geom_hline(yintercept=0.98, linetype="dashed", color = "black")
  
